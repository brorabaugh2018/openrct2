var searchData=
[
  ['datetime64_32018',['datetime64',['../common_8h.html#acc57ae263e2eabfe525f4782b0896b0b',1,'common.h']]],
  ['difference_5ftype_32019',['difference_type',['../class_circular_buffer.html#a6bb9c019eeef51f78d7a153b9af98455',1,'CircularBuffer::difference_type()'],['../class_ride_manager_1_1_iterator.html#aa4dca36d1a61da5ab9a88a7d9a4ba30d',1,'RideManager::Iterator::difference_type()']]],
  ['dirbase_5fvalues_32020',['DIRBASE_VALUES',['../namespace_open_r_c_t2.html#a1b35f2e17140de9a40641583135859da',1,'OpenRCT2']]],
  ['direction_32021',['Direction',['../_location_8hpp.html#a8c81e50441f7edac81fb070d9a36ff25',1,'Location.hpp']]],
  ['drawrainfunc_32022',['DrawRainFunc',['../namespace_open_r_c_t2_1_1_drawing.html#a7e177418c4fc2f8ef7323c8b68b1b8f7',1,'OpenRCT2::Drawing']]]
];
