var searchData=
[
  ['dat_5fname_5flength_45664',['DAT_NAME_LENGTH',['../_object_limits_8h.html#a284dd6389c7794d9d4c6269331d59073',1,'ObjectLimits.h']]],
  ['datetime64_5fmin_45665',['DATETIME64_MIN',['../common_8h.html#af6c25edd7a3fc53199eb2df7eea0cf91',1,'common.h']]],
  ['debug_5flevel_5f1_45666',['DEBUG_LEVEL_1',['../_diagnostic_8h.html#a4bd6842e68bd25a522617de54c6fcd8d',1,'Diagnostic.h']]],
  ['debug_5flevel_5f2_45667',['DEBUG_LEVEL_2',['../_diagnostic_8h.html#a294a095c50b175374ac5f929f2cd75fc',1,'Diagnostic.h']]],
  ['debug_5flevel_5f3_45668',['DEBUG_LEVEL_3',['../_diagnostic_8h.html#a2b16e70396a4c6e4a020a47a3bca4590',1,'Diagnostic.h']]],
  ['decrypt_5fmoney_45669',['DECRYPT_MONEY',['../_park_8h.html#a397192a94d1c2f4c60162c96b97696d2',1,'Park.h']]],
  ['default_5fenabled_5fwidgets_45670',['DEFAULT_ENABLED_WIDGETS',['../_about_8cpp.html#a37aa77e0a60338550a46b93f13c1cf6b',1,'About.cpp']]],
  ['default_5fflat_5fride_5fcolour_5fpreset_45671',['DEFAULT_FLAT_RIDE_COLOUR_PRESET',['../_ride_data_8cpp.html#a98f0e8bd1b284807870766b8b7c11ffb',1,'RideData.cpp']]],
  ['default_5fnum_5fautosaves_5fto_5fkeep_45672',['DEFAULT_NUM_AUTOSAVES_TO_KEEP',['../_scenario_8h.html#a523c976ec4840d492baaa5cb4f40c088',1,'Scenario.h']]],
  ['default_5fstall_5fcolour_5fpreset_45673',['DEFAULT_STALL_COLOUR_PRESET',['../_ride_data_8cpp.html#a319a8ac374a85724576ec6fdb3976dda',1,'RideData.cpp']]],
  ['define_5fgame_5faction_45674',['DEFINE_GAME_ACTION',['../_game_action_8h.html#a3a283a62223b841cbd4fb3521bf9ea39',1,'GameAction.h']]],
  ['define_5fscenario_5ftitle_5fdesc_5fgroup_45675',['DEFINE_SCENARIO_TITLE_DESC_GROUP',['../_scenario_sources_8cpp.html#a7fc45f4e1b0f1663cbfad08679db10da',1,'ScenarioSources.cpp']]],
  ['definecommand_45676',['DefineCommand',['../_command_line_8hpp.html#a8baf179dd94670d4d505e93ee4ca0986',1,'CommandLine.hpp']]],
  ['definesubcommand_45677',['DefineSubCommand',['../_command_line_8hpp.html#a45c9af44741ffd75680fcd8985bb8138',1,'CommandLine.hpp']]],
  ['diagnostic_5flog_5fmacro_45678',['diagnostic_log_macro',['../_diagnostic_8h.html#a9e3161c9c109df722af63cdfc877755f',1,'Diagnostic.h']]],
  ['disable_5fopt_45679',['DISABLE_OPT',['../_addresses_8cpp.html#a31e75a7f149233a7af8533fdd5a3127c',1,'Addresses.cpp']]],
  ['dllexport_45680',['DLLEXPORT',['../openrct2-dll_8cpp.html#a808e08638be3cba36e36759e5b150de0',1,'openrct2-dll.cpp']]],
  ['dllimport_45681',['DLLIMPORT',['../openrct2-win_8cpp.html#aae8fdf6bcc88c172ca8a75ad80f17a95',1,'openrct2-win.cpp']]],
  ['downtime_5fhistory_5fsize_45682',['DOWNTIME_HISTORY_SIZE',['../_ride_8h.html#a1dfd9aab9076d1df5d0de0809e4caa92',1,'Ride.h']]],
  ['drawrlespritehelper1_45683',['DrawRLESpriteHelper1',['../_drawing_fast_8cpp.html#af2adb0c2be4ac09c608ba9875ebb1769',1,'DrawingFast.cpp']]],
  ['drawrlespritehelper2_45684',['DrawRLESpriteHelper2',['../_drawing_fast_8cpp.html#ad67621bcecb0d3c414d9ac529800da02',1,'DrawingFast.cpp']]],
  ['dropdown_5fformat_5fcolour_5fpicker_45685',['DROPDOWN_FORMAT_COLOUR_PICKER',['../_dropdown_8h.html#a0d0fc2665112b809246108e1b3867fc3',1,'Dropdown.h']]],
  ['dropdown_5fformat_5fland_5fpicker_45686',['DROPDOWN_FORMAT_LAND_PICKER',['../_dropdown_8h.html#a7e076110636d144e4bcf3ffa52aefd30',1,'Dropdown.h']]],
  ['dropdown_5fitem_5fheight_45687',['DROPDOWN_ITEM_HEIGHT',['../_dropdown_8cpp.html#ac6afaca9704443ad40ba959803a6e4a9',1,'Dropdown.cpp']]],
  ['dropdown_5fitems_5fmax_5fsize_45688',['DROPDOWN_ITEMS_MAX_SIZE',['../_dropdown_8h.html#a260065e78093a1eb41dc01864477e1e8',1,'Dropdown.h']]],
  ['dropdown_5fseparator_45689',['DROPDOWN_SEPARATOR',['../_dropdown_8h.html#aaf1a8fa71037d2840e9358b1bd2c7688',1,'Dropdown.h']]],
  ['dropdown_5ftext_5fmax_5frows_45690',['DROPDOWN_TEXT_MAX_ROWS',['../_dropdown_8cpp.html#a0bd0a590c0770e4e649548f08cf46d9c',1,'Dropdown.cpp']]],
  ['ds_5ftag_45691',['DS_TAG',['../_data_serialiser_tag_8h.html#a02ed9b9b138e06d4cd105abea1f97416',1,'DataSerialiserTag.h']]],
  ['dsbpan_5fleft_45692',['DSBPAN_LEFT',['../_audio_mixer_8h.html#a61203b0c628524ce7f071eea0504ecf9',1,'AudioMixer.h']]],
  ['dsbpan_5fright_45693',['DSBPAN_RIGHT',['../_audio_mixer_8h.html#af85bfa5a595f135bd5b1874133303e6a',1,'AudioMixer.h']]]
];
