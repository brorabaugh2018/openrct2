var searchData=
[
  ['validate_5fglobal_5fwidx_46222',['validate_global_widx',['../interface_2_window_8h.html#af63c8868a8f1021dc190a4b5fda9d27a',1,'Window.h']]],
  ['vehicle_5finvalid_5fid_46223',['VEHICLE_INVALID_ID',['../_vehicle_8cpp.html#a30ef414615d869b6c0d564e12661ae4b',1,'Vehicle.cpp']]],
  ['vehicle_5fmax_5fspin_5fspeed_46224',['VEHICLE_MAX_SPIN_SPEED',['../_vehicle_8cpp.html#ac41be1c89ca03b63778bee5acd6041c8',1,'Vehicle.cpp']]],
  ['vehicle_5fmax_5fspin_5fspeed_5ffor_5fstopping_46225',['VEHICLE_MAX_SPIN_SPEED_FOR_STOPPING',['../_vehicle_8cpp.html#ac3492ecb1f9322e766c21550011db3b3',1,'Vehicle.cpp']]],
  ['vehicle_5fmax_5fspin_5fspeed_5fwater_5fride_46226',['VEHICLE_MAX_SPIN_SPEED_WATER_RIDE',['../_vehicle_8cpp.html#ab6e0fce69cbf9ff0be3e404c3b60dbab',1,'Vehicle.cpp']]],
  ['vehicle_5fseat_5fnum_5fmask_46227',['VEHICLE_SEAT_NUM_MASK',['../_vehicle_8h.html#aea93a329842dede0ab5d65ddcbf2c938',1,'Vehicle.h']]],
  ['vehicle_5fseat_5fpair_5fflag_46228',['VEHICLE_SEAT_PAIR_FLAG',['../_vehicle_8h.html#adb13473baa25049620992df9f30901f0',1,'Vehicle.h']]],
  ['vehicle_5fstopping_5fspin_5fspeed_46229',['VEHICLE_STOPPING_SPIN_SPEED',['../_vehicle_8cpp.html#a2bc265c1bac4c69997cbe395f4138a75',1,'Vehicle.cpp']]],
  ['vertical_5fgroupbox_5fpadding_46230',['VERTICAL_GROUPBOX_PADDING',['../ui_2windows_2_tile_inspector_8cpp.html#a00f299b142518c52ead3da4d9eeb6d54',1,'TileInspector.cpp']]],
  ['viewport_5ffocus_5ftype_5fmask_46231',['VIEWPORT_FOCUS_TYPE_MASK',['../interface_2_window_8h.html#a60ebae48f0ebbdd5e87ad6e3c61e11a8',1,'Window.h']]],
  ['viewport_5ffocus_5fy_5fmask_46232',['VIEWPORT_FOCUS_Y_MASK',['../interface_2_window_8h.html#a9b7c7cca7478fefcd4aaf96fc970304b',1,'Window.h']]]
];
