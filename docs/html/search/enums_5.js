var searchData=
[
  ['file_5fdialog_5ftype_32123',['FILE_DIALOG_TYPE',['../namespace_open_r_c_t2_1_1_ui.html#af8516aab004c33cd9ad44eb75b01dca5',1,'OpenRCT2::Ui']]],
  ['file_5fmenu_5fddidx_32124',['FILE_MENU_DDIDX',['../_top_toolbar_8cpp.html#a4531e55870f24e08648fc000390bbe39',1,'TopToolbar.cpp']]],
  ['file_5ftype_32125',['FILE_TYPE',['../_file_classifier_8h.html#a96c970a7db1d402a64b518ec5146312a',1,'FileClassifier.h']]],
  ['filedialog_5ftype_32126',['FILEDIALOG_TYPE',['../platform_8h.html#abe82029250ea3d60a3dcca8734dd2d39',1,'platform.h']]],
  ['filter_5fpalette_5fid_32127',['FILTER_PALETTE_ID',['../_drawing_8h.html#a156372da72a8b86244cb8816fa2d1da9',1,'Drawing.h']]],
  ['fullscreen_5fmode_32128',['FULLSCREEN_MODE',['../namespace_open_r_c_t2_1_1_ui.html#afb69322bf779a07f5a7d4e324cf7a5eb',1,'OpenRCT2::Ui']]]
];
