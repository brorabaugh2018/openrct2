var searchData=
[
  ['l5_5fspin_33212',['L5_SPIN',['../_vehicle_8cpp.html#a341163acd830e6c164e03eff6bc76e8ea76b06dd996960945f056bc69c85caf0b',1,'Vehicle.cpp']]],
  ['l7_5fspin_33213',['L7_SPIN',['../_vehicle_8cpp.html#a341163acd830e6c164e03eff6bc76e8ea76ba47c5bd55a5634bbf98e0ad9a656d',1,'Vehicle.cpp']]],
  ['l8_5fspin_33214',['L8_SPIN',['../_vehicle_8cpp.html#a341163acd830e6c164e03eff6bc76e8ea3693895afd29bcd2809eb748e3712e8a',1,'Vehicle.cpp']]],
  ['l9_5fspin_33215',['L9_SPIN',['../_vehicle_8cpp.html#a341163acd830e6c164e03eff6bc76e8eaca67071ad362d7252b735440a70ef2c5',1,'Vehicle.cpp']]],
  ['landscape_33216',['LANDSCAPE',['../namespace_open_r_c_t2.html#a9359faa178ff2f83dea90fc761384fd2a20287e25ffb71f92af9803e4c3a53928',1,'OpenRCT2']]],
  ['language_33217',['LANGUAGE',['../namespace_open_r_c_t2.html#a9359faa178ff2f83dea90fc761384fd2a5b64a5253f843da2950972747ac9a3b0',1,'OpenRCT2']]],
  ['language_5farabic_33218',['LANGUAGE_ARABIC',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a6056de79f87dfdc661c28482275afdfb',1,'Language.h']]],
  ['language_5fcatalan_33219',['LANGUAGE_CATALAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80ac36c79c9142a2f3df7088131b2a4f16e',1,'Language.h']]],
  ['language_5fchinese_5fsimplified_33220',['LANGUAGE_CHINESE_SIMPLIFIED',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a7cb7c17354cc5329d8df4678039ccd69',1,'Language.h']]],
  ['language_5fchinese_5ftraditional_33221',['LANGUAGE_CHINESE_TRADITIONAL',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80af1ff1c1b407aab6bfc570df7e0b117a4',1,'Language.h']]],
  ['language_5fcount_33222',['LANGUAGE_COUNT',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80ae646c233db6fdd2b280608defe1a07c4',1,'Language.h']]],
  ['language_5fczech_33223',['LANGUAGE_CZECH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80ab1662332de6af381ac9756a4e18bef0e',1,'Language.h']]],
  ['language_5fdanish_33224',['LANGUAGE_DANISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80ab07a3a869e2028f97c5bb1e7c81744f6',1,'Language.h']]],
  ['language_5fdutch_33225',['LANGUAGE_DUTCH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a143ae0b85223b330bb96178067731f43',1,'Language.h']]],
  ['language_5fenglish_5fuk_33226',['LANGUAGE_ENGLISH_UK',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80aba3b96466addac46236d692f051c6ef3',1,'Language.h']]],
  ['language_5fenglish_5fus_33227',['LANGUAGE_ENGLISH_US',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a6b2cca05c49e07dadc07b6bd9dc2057c',1,'Language.h']]],
  ['language_5ffinnish_33228',['LANGUAGE_FINNISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80abd262188ab355b86881dfbfc46d9f63d',1,'Language.h']]],
  ['language_5ffrench_33229',['LANGUAGE_FRENCH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80aaa92e670e87bd38b41477c687aa9bc29',1,'Language.h']]],
  ['language_5fgerman_33230',['LANGUAGE_GERMAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a937e39bfe9c8739576918bea9e84d503',1,'Language.h']]],
  ['language_5fhungarian_33231',['LANGUAGE_HUNGARIAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a56929e93cfd29b5d29f0677bf0cbd93f',1,'Language.h']]],
  ['language_5fitalian_33232',['LANGUAGE_ITALIAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a2b76229caa8e347ea2fb2105f9b95b5a',1,'Language.h']]],
  ['language_5fjapanese_33233',['LANGUAGE_JAPANESE',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a4477314872374ff5cacfac3d1677cae4',1,'Language.h']]],
  ['language_5fkorean_33234',['LANGUAGE_KOREAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a5f40b92cbcbb750003a23153f7cd745e',1,'Language.h']]],
  ['language_5fnorwegian_33235',['LANGUAGE_NORWEGIAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80abbf6d9acd8eba8f422681e4f50e7230f',1,'Language.h']]],
  ['language_5fpolish_33236',['LANGUAGE_POLISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a539956374261f872d323e8b33653455c',1,'Language.h']]],
  ['language_5fportuguese_5fbr_33237',['LANGUAGE_PORTUGUESE_BR',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80af9d13616ebd0dd8e06c18a22c43daa84',1,'Language.h']]],
  ['language_5frussian_33238',['LANGUAGE_RUSSIAN',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a1bb521c477768a93f551f779755c954d',1,'Language.h']]],
  ['language_5fspanish_33239',['LANGUAGE_SPANISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80acb152f19692738f6d1257fab594f5066',1,'Language.h']]],
  ['language_5fswedish_33240',['LANGUAGE_SWEDISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a48b75f0da429ac3838296d23b7087ca6',1,'Language.h']]],
  ['language_5fturkish_33241',['LANGUAGE_TURKISH',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80a459928218710800594c7d8b222dfa37c',1,'Language.h']]],
  ['language_5fundefined_33242',['LANGUAGE_UNDEFINED',['../_language_8h.html#a4b4213b1f9d54da7c7e36bde32fc7c80ada54eaac39cebb65862ee20d21438436',1,'Language.h']]],
  ['large_5fscenery_5fflag_5f3d_5ftext_33243',['LARGE_SCENERY_FLAG_3D_TEXT',['../_scenery_8h.html#a1e6405a672d35cfdf2ca24884d9ada3eaf8a6eb62ab183697d9a82247045f2f26',1,'Scenery.h']]],
  ['large_5fscenery_5fflag_5fanimated_33244',['LARGE_SCENERY_FLAG_ANIMATED',['../_scenery_8h.html#a1e6405a672d35cfdf2ca24884d9ada3ea97e14cdbbaefc0fd0f876c9bebcc060b',1,'Scenery.h']]],
  ['large_5fscenery_5fflag_5fhas_5fprimary_5fcolour_33245',['LARGE_SCENERY_FLAG_HAS_PRIMARY_COLOUR',['../_scenery_8h.html#a1e6405a672d35cfdf2ca24884d9ada3eac04beaade9b4ebd8c6f771a9af3f7749',1,'Scenery.h']]],
  ['large_5fscenery_5fflag_5fhas_5fsecondary_5fcolour_33246',['LARGE_SCENERY_FLAG_HAS_SECONDARY_COLOUR',['../_scenery_8h.html#a1e6405a672d35cfdf2ca24884d9ada3eaa0512d7290edb140292011d00a9b5e80',1,'Scenery.h']]],
  ['large_5fscenery_5fflag_5fphotogenic_33247',['LARGE_SCENERY_FLAG_PHOTOGENIC',['../_scenery_8h.html#a1e6405a672d35cfdf2ca24884d9ada3ea8f1e8f31bf4db4fca1d1d191b30061ff',1,'Scenery.h']]],
  ['large_5fscenery_5ftext_5fflag_5ftwo_5fline_33248',['LARGE_SCENERY_TEXT_FLAG_TWO_LINE',['../_scenery_8h.html#a35b16ff145e0c1662d7d2fd590628222abeeebc5c0fd77a2a094227f532078fa6',1,'Scenery.h']]],
  ['large_5fscenery_5ftext_5fflag_5fvertical_33249',['LARGE_SCENERY_TEXT_FLAG_VERTICAL',['../_scenery_8h.html#a35b16ff145e0c1662d7d2fd590628222a31b04c0c4e56ce3c963e034ae02e05aa',1,'Scenery.h']]],
  ['large_5fscenery_5ftile_5fflag_5fallow_5fsupports_5fabove_33250',['LARGE_SCENERY_TILE_FLAG_ALLOW_SUPPORTS_ABOVE',['../_scenery_8h.html#a9fa4f6a0a65da28017d3570a401dc67aa12fa4b2ef6a036ac2c6914250a3418f7',1,'Scenery.h']]],
  ['large_5fscenery_5ftile_5fflag_5fno_5fsupports_33251',['LARGE_SCENERY_TILE_FLAG_NO_SUPPORTS',['../_scenery_8h.html#a9fa4f6a0a65da28017d3570a401dc67aa10d42f77806cf66ca65812dababb6bfe',1,'Scenery.h']]],
  ['largescenery_33252',['LargeScenery',['../_r_c_t12_8h.html#a59bff1292891d44436b9461101cda2b0a60059038013939b8b98fbe1f659b4b1c',1,'LargeScenery():&#160;RCT12.h'],['../_tile_element_8h.html#a85b2e6b80be25fcf3f8dec2a309a49f3a60059038013939b8b98fbe1f659b4b1c',1,'LargeScenery():&#160;TileElement.h']]],
  ['laugh1_33253',['Laugh1',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca3b9df15ab82cdd6115db44ce939fbd53',1,'audio.h']]],
  ['laugh2_33254',['Laugh2',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bcae9cc6e0a9321eab798feab12736536d6',1,'audio.h']]],
  ['laugh3_33255',['Laugh3',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca236a20cb6722836c1fb4e92568cc8fe8',1,'audio.h']]],
  ['layingoutwater_33256',['LayingOutWater',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca0ee01e1efb22c5306d0a29934cb65c5c',1,'audio.h']]],
  ['left_33257',['LEFT',['../_text_8h.html#aa56f1a82069b5feeadbb4591cb3e474fa684d325a7303f52e64011467ff5c5758',1,'LEFT():&#160;Text.h'],['../_screenshot_8cpp.html#a424a64da753a3cd5e96ab8d0553a04c4a684d325a7303f52e64011467ff5c5758',1,'LEFT():&#160;Screenshot.cpp']]],
  ['liftarrow_33258',['LiftArrow',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca0085d8cc764d27aba39ba8fee2b2fb40',1,'audio.h']]],
  ['liftbm_33259',['LiftBM',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bcaef280ececb1e7f180d50854411801eed',1,'audio.h']]],
  ['liftclassic_33260',['LiftClassic',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca582443832fb32c2d2f0d545ed761cb25',1,'audio.h']]],
  ['liftfrictionwheels_33261',['LiftFrictionWheels',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bcaaf7826d6e74163b33704a5eb3e8c9268',1,'audio.h']]],
  ['lifthillspeed_33262',['LiftHillSpeed',['../_ride_set_setting_8hpp.html#a387cceed24c652adc60c667bb72a6ca6a85d3789dc912f1133c512366f273f9d6',1,'RideSetSetting.hpp']]],
  ['liftwildmouse_33263',['LiftWildMouse',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca617ee45e18215775d6b3b953e2e90bd3',1,'audio.h']]],
  ['liftwood_33264',['LiftWood',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca06f82bf8f05d0ebbdc6aa659d1eb367b',1,'audio.h']]],
  ['litter_5ftype_5fempty_5fbottle_33265',['LITTER_TYPE_EMPTY_BOTTLE',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0faa201b85f574ec2a14367ac88e2777bc3',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fbowl_5fblue_33266',['LITTER_TYPE_EMPTY_BOWL_BLUE',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0facf0d6a156c80322b471917e518cfd7c9',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fbowl_5fred_33267',['LITTER_TYPE_EMPTY_BOWL_RED',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fa4e791e292ebefd5235e7fa86a0e7cef4',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fbox_33268',['LITTER_TYPE_EMPTY_BOX',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fa8f841e57067cbcfb380c3101bb3ea2de',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fburger_5fbox_33269',['LITTER_TYPE_EMPTY_BURGER_BOX',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fad3cb876007875c2f1ad9c96963b2c15b',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fcan_33270',['LITTER_TYPE_EMPTY_CAN',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fafb84b79564e1947fa2fefd2777bf5474',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fcup_33271',['LITTER_TYPE_EMPTY_CUP',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fa7080ccf4d6777ec4c57dcb2371b39d87',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fdrink_5fcarton_33272',['LITTER_TYPE_EMPTY_DRINK_CARTON',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fab1359c53d524a58f8edb08a1c96e68f9',1,'Sprite.h']]],
  ['litter_5ftype_5fempty_5fjuice_5fcup_33273',['LITTER_TYPE_EMPTY_JUICE_CUP',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fa4fc9966c13af7706c303c99342ccbcf6',1,'Sprite.h']]],
  ['litter_5ftype_5frubbish_33274',['LITTER_TYPE_RUBBISH',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fa0078f2655885308ff8a4edac3cce0dea',1,'Sprite.h']]],
  ['litter_5ftype_5fsick_33275',['LITTER_TYPE_SICK',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0faebcbbab6a17689dcd7c149aba00c36ff',1,'Sprite.h']]],
  ['litter_5ftype_5fsick_5falt_33276',['LITTER_TYPE_SICK_ALT',['../_sprite_8h.html#a7f32b412e3b987c5e865c6578e899a0fad782837144ba6ecf406100201d4829ee',1,'Sprite.h']]],
  ['loadsavetype_5fgame_33277',['LOADSAVETYPE_GAME',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26ab618b5506cdc6cf184948ec9ebe12cb0',1,'Window.h']]],
  ['loadsavetype_5fheightmap_33278',['LOADSAVETYPE_HEIGHTMAP',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26ad51ec02ace435180d3e4fb945291fadb',1,'Window.h']]],
  ['loadsavetype_5flandscape_33279',['LOADSAVETYPE_LANDSCAPE',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26a2b09e669ed8f51796f1d18f4d682d164',1,'Window.h']]],
  ['loadsavetype_5fload_33280',['LOADSAVETYPE_LOAD',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26adf171dba5df66313a9e6ebd55be7f383',1,'Window.h']]],
  ['loadsavetype_5fsave_33281',['LOADSAVETYPE_SAVE',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26a80a18fb0743d6b7b06fb0896915014d0',1,'Window.h']]],
  ['loadsavetype_5fscenario_33282',['LOADSAVETYPE_SCENARIO',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26ab153784ab79cf1b6bf66905c8bd08bfe',1,'Window.h']]],
  ['loadsavetype_5ftrack_33283',['LOADSAVETYPE_TRACK',['../interface_2_window_8h.html#af8654c714018c6241d9ebd01f3fb5a26aac447d4149d22e3a523db9619915b357',1,'Window.h']]],
  ['log_5fchat_33284',['LOG_CHAT',['../namespace_open_r_c_t2.html#a9359faa178ff2f83dea90fc761384fd2ae2d2668e6ce4cbb2756582403b0fe4cd',1,'OpenRCT2']]],
  ['log_5fdesyncs_33285',['LOG_DESYNCS',['../namespace_open_r_c_t2.html#a9359faa178ff2f83dea90fc761384fd2a94f3ccbeb96d127ef30a09d2e8de31aa',1,'OpenRCT2']]],
  ['log_5fserver_33286',['LOG_SERVER',['../namespace_open_r_c_t2.html#a9359faa178ff2f83dea90fc761384fd2a485d536947029f4a138b2bd766c5ff58',1,'OpenRCT2']]],
  ['lr_5fspin_33287',['LR_SPIN',['../_vehicle_8cpp.html#a341163acd830e6c164e03eff6bc76e8ea747d92e36bf4259c59f83920d904664c',1,'Vehicle.cpp']]]
];
