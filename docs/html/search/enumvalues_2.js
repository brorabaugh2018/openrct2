var searchData=
[
  ['cache_32365',['CACHE',['../namespace_open_r_c_t2.html#a22b7f516bd872b4e365d64dcd94d86aba08e1be4b785493d9fc2ae52c5ba9ab2e',1,'OpenRCT2']]],
  ['cache_5fobjects_32366',['CACHE_OBJECTS',['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0a444a806204d6254eb1ea3622bb503791',1,'OpenRCT2']]],
  ['cache_5fscenarios_32367',['CACHE_SCENARIOS',['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0a19d056187951729ebcc925ee81fe24ed',1,'OpenRCT2']]],
  ['cache_5ftracks_32368',['CACHE_TRACKS',['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0a11d98374cec2829a1fcadcda7b78224c',1,'OpenRCT2']]],
  ['campaign_5factive_5fflag_32369',['CAMPAIGN_ACTIVE_FLAG',['../_marketing_8h.html#ab39a415800ebd0d977c477376649649baa4ec8c86e468a97541cca0377eb134c8',1,'Marketing.h']]],
  ['campaign_5ffirst_5fweek_5fflag_32370',['CAMPAIGN_FIRST_WEEK_FLAG',['../_marketing_8h.html#ab39a415800ebd0d977c477376649649ba0fa3350e30d9a0b27f9b508b14315e3c',1,'Marketing.h']]],
  ['can_5fgrow_32371',['CAN_GROW',['../_terrain_surface_object_8h.html#a2c72c34494083f4dd2a12c32e754f187a249ee6e34ab9d846b5137a4d69208284',1,'TerrainSurfaceObject.h']]],
  ['cancel_32372',['Cancel',['../_peep_pickup_action_8hpp.html#aaa88cb88c4c88520d83ac6ba0c72aba8aea4788705e6873b424c65e91c2846b19',1,'PeepPickupAction.hpp']]],
  ['cassert_32373',['CASSERT',['../_guard_8hpp.html#a11e9a2b61693e04718579e7b99104c07af9ab9a13615185f23df7f142c7bc06dd',1,'Guard.hpp']]],
  ['centre_32374',['CENTRE',['../_text_8h.html#aa56f1a82069b5feeadbb4591cb3e474fa861d9b366d4c6f701fbc59ba9ab7bdda',1,'Text.h']]],
  ['changelog_32375',['CHANGELOG',['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0ad3bb3391c79904494c60ee2ac2f33070',1,'OpenRCT2']]],
  ['chat_5finput_5fclose_32376',['CHAT_INPUT_CLOSE',['../_chat_8h.html#a8724d0c9391def7a60daa1766000cefda2faec9f27e0ba0ecfc8452107f37c51e',1,'Chat.h']]],
  ['chat_5finput_5fnone_32377',['CHAT_INPUT_NONE',['../_chat_8h.html#a8724d0c9391def7a60daa1766000cefda738c0f2769497f64425bb7dcb0cb62f1',1,'Chat.h']]],
  ['chat_5finput_5fsend_32378',['CHAT_INPUT_SEND',['../_chat_8h.html#a8724d0c9391def7a60daa1766000cefda758d9dacbacbca3ca8071efd26744364',1,'Chat.h']]],
  ['chunk_5fencoding_5fnone_32379',['CHUNK_ENCODING_NONE',['../_sawyer_coding_8h.html#af82906a02e18874fe91588dabf28f387a0ff5c431ce398d333218f776dae28aae',1,'SawyerCoding.h']]],
  ['chunk_5fencoding_5frle_32380',['CHUNK_ENCODING_RLE',['../_sawyer_coding_8h.html#af82906a02e18874fe91588dabf28f387ab7df2de5312324c152a1ba761c27dc7c',1,'SawyerCoding.h']]],
  ['chunk_5fencoding_5frlecompressed_32381',['CHUNK_ENCODING_RLECOMPRESSED',['../_sawyer_coding_8h.html#af82906a02e18874fe91588dabf28f387a3a036b8a3ac64bf174966673786cdb9f',1,'SawyerCoding.h']]],
  ['chunk_5fencoding_5frotate_32382',['CHUNK_ENCODING_ROTATE',['../_sawyer_coding_8h.html#af82906a02e18874fe91588dabf28f387abc0336ce9d58dd7c4e7aa92dd15a65af',1,'SawyerCoding.h']]],
  ['clearall_32383',['ClearAll',['../_network_modify_group_action_8hpp.html#a092502b840c7cf2c9379b470cfdd83fda7aaf620fe818489b59d52585ff04d995',1,'NetworkModifyGroupAction.hpp']]],
  ['clearloan_32384',['ClearLoan',['../_cheats_8h.html#a89b6419984cb46d3ae8f5bfe41c742aea3c652a8918e0d8f946ff9bda45b997cb',1,'Cheats.h']]],
  ['click1_32385',['Click1',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca44e5d453fd360a41d7bc2885bdbd0fe4',1,'audio.h']]],
  ['click2_32386',['Click2',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bcaba5e545f6acb235f5274a3f223cb05a2',1,'audio.h']]],
  ['click3_32387',['Click3',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca81f386645255e870888b015d00c8b4a5',1,'audio.h']]],
  ['climate_5fcold_32388',['CLIMATE_COLD',['../_climate_8h.html#a0112ff93378dc570ab64ab32d6132a1ea82224321a27acf52229cad57a8c0111b',1,'Climate.h']]],
  ['climate_5fcool_5fand_5fwet_32389',['CLIMATE_COOL_AND_WET',['../_climate_8h.html#a0112ff93378dc570ab64ab32d6132a1ea98743d8cdf44ca2b06500ebae129f9ba',1,'Climate.h']]],
  ['climate_5fcount_32390',['CLIMATE_COUNT',['../_climate_8h.html#a0112ff93378dc570ab64ab32d6132a1ea2d971ec0d6759a85aa4eb760f4f32953',1,'Climate.h']]],
  ['climate_5fhot_5fand_5fdry_32391',['CLIMATE_HOT_AND_DRY',['../_climate_8h.html#a0112ff93378dc570ab64ab32d6132a1eac6f6fa0f14608697a67f6c4f13ebc378',1,'Climate.h']]],
  ['climate_5fwarm_32392',['CLIMATE_WARM',['../_climate_8h.html#a0112ff93378dc570ab64ab32d6132a1eaf6515f1357b3a23d4b58414dfd4b2066',1,'Climate.h']]],
  ['close_32393',['Close',['../_park_set_parameter_action_8hpp.html#a8a8b8e901d3f1d0cec7e1f0ecaed3dcead3d2e617335f08df83599665eef8a418',1,'ParkSetParameterAction.hpp']]],
  ['closesaveprompt_32394',['CloseSavePrompt',['../_load_or_quit_action_8hpp.html#a2759e920728039727f58e755d4f40d03a4fc57b6169379b4332c184f0ce6ec7d3',1,'LoadOrQuitAction.hpp']]],
  ['closest_32395',['CLOSEST',['../class_open_r_c_t2_1_1_drawing_1_1_image_importer.html#a9685ad95f032d04450020912053d51f0ae9c32f09a44ae1ae34e42d53502fab77',1,'OpenRCT2::Drawing::ImageImporter']]],
  ['cmdline_5ftype_5finteger_32396',['CMDLINE_TYPE_INTEGER',['../_command_line_8hpp.html#abc6126af1d45847bc59afa0aa3216b04a8c96f5a38051c7a3f3dff7f26bb94714',1,'CommandLine.hpp']]],
  ['cmdline_5ftype_5freal_32397',['CMDLINE_TYPE_REAL',['../_command_line_8hpp.html#abc6126af1d45847bc59afa0aa3216b04a2a7b01ce1a8620c1fc50cd550442f968',1,'CommandLine.hpp']]],
  ['cmdline_5ftype_5fstring_32398',['CMDLINE_TYPE_STRING',['../_command_line_8hpp.html#abc6126af1d45847bc59afa0aa3216b04aeab11b51af3a3007b9ff6a5a4d42b615',1,'CommandLine.hpp']]],
  ['cmdline_5ftype_5fswitch_32399',['CMDLINE_TYPE_SWITCH',['../_command_line_8hpp.html#abc6126af1d45847bc59afa0aa3216b04ad4fe59b9600c90a0bc7f071aa7e5ab9e',1,'CommandLine.hpp']]],
  ['colour_5faquamarine_32400',['COLOUR_AQUAMARINE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a65b63decb28de50d1dbe582fe8d20f7c',1,'Colour.h']]],
  ['colour_5fblack_32401',['COLOUR_BLACK',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a6febc7fbdb3de48f885d55ce583e45a2',1,'Colour.h']]],
  ['colour_5fbordeaux_5fred_32402',['COLOUR_BORDEAUX_RED',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3acdae684065d599c6c86d4ac3caa30bf7',1,'Colour.h']]],
  ['colour_5fbright_5fgreen_32403',['COLOUR_BRIGHT_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a4969934b4d5ec43a48d81bed6ae4bba6',1,'Colour.h']]],
  ['colour_5fbright_5fpink_32404',['COLOUR_BRIGHT_PINK',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ad79f1c85d06ca9eb1cf55793f0298fc9',1,'Colour.h']]],
  ['colour_5fbright_5fpurple_32405',['COLOUR_BRIGHT_PURPLE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a2ab3df39f04cce1302dcc723241ffb39',1,'Colour.h']]],
  ['colour_5fbright_5fred_32406',['COLOUR_BRIGHT_RED',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a978bee81a83cdc616f6d947a1391ef98',1,'Colour.h']]],
  ['colour_5fbright_5fyellow_32407',['COLOUR_BRIGHT_YELLOW',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a8740c7c2d2d1e92a17415dcb0df75975',1,'Colour.h']]],
  ['colour_5fcount_32408',['COLOUR_COUNT',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ab1371ca447260d2332b13b46a423a56b',1,'Colour.h']]],
  ['colour_5fdark_5fblue_32409',['COLOUR_DARK_BLUE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a05db8ff7f3dec0118e8cddeac9b7ca80',1,'Colour.h']]],
  ['colour_5fdark_5fbrown_32410',['COLOUR_DARK_BROWN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3aafd74699cf646fbab4337aeff84fd409',1,'Colour.h']]],
  ['colour_5fdark_5fgreen_32411',['COLOUR_DARK_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a17e15e1f435793885b5c711226770539',1,'Colour.h']]],
  ['colour_5fdark_5folive_5fgreen_32412',['COLOUR_DARK_OLIVE_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ac4db197048b0d6cbf7a70e839e3939d7',1,'Colour.h']]],
  ['colour_5fdark_5forange_32413',['COLOUR_DARK_ORANGE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3afe24f3d18196ddb9df82ea0ea2f41de8',1,'Colour.h']]],
  ['colour_5fdark_5fpink_32414',['COLOUR_DARK_PINK',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a6102f4d55cc72e8b1a35fb3d84aab548',1,'Colour.h']]],
  ['colour_5fdark_5fpurple_32415',['COLOUR_DARK_PURPLE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ac7cd5c0ef82f64990795e0537298d9ed',1,'Colour.h']]],
  ['colour_5fdark_5fwater_32416',['COLOUR_DARK_WATER',['../_colour_8h.html#ae8a3b6a5d0d3244ed73924ab2421a0d0af4014f5c22f8c70c324e91702338e226',1,'Colour.h']]],
  ['colour_5fdark_5fyellow_32417',['COLOUR_DARK_YELLOW',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a9a83f36778636e602dea232105693046',1,'Colour.h']]],
  ['colour_5fflag_5f8_32418',['COLOUR_FLAG_8',['../_colour_8h.html#afb730582952b7ceec73d7dc9bf7bef39a7ccc8ff7dede9fda4da8c07c7f6b4359',1,'Colour.h']]],
  ['colour_5fflag_5finset_32419',['COLOUR_FLAG_INSET',['../_colour_8h.html#afb730582952b7ceec73d7dc9bf7bef39a6e979308a6b12cddbdb05caa85f830a1',1,'Colour.h']]],
  ['colour_5fflag_5foutline_32420',['COLOUR_FLAG_OUTLINE',['../_colour_8h.html#afb730582952b7ceec73d7dc9bf7bef39ad37b62719b9716342ea32ee57d4f4ecb',1,'Colour.h']]],
  ['colour_5fflag_5ftranslucent_32421',['COLOUR_FLAG_TRANSLUCENT',['../_colour_8h.html#afb730582952b7ceec73d7dc9bf7bef39a6a9ea5334a99c79468fd0ef4b0c16014',1,'Colour.h']]],
  ['colour_5fgrey_32422',['COLOUR_GREY',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a9d4b57dd228d1b5ccfa90a5b53b5873f',1,'Colour.h']]],
  ['colour_5ficy_5fblue_32423',['COLOUR_ICY_BLUE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3aeebec3b5a2a33d835fc8ace3f86b579d',1,'Colour.h']]],
  ['colour_5fkey_5fcash_5fmachine_32424',['COLOUR_KEY_CASH_MACHINE',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0faf75ab1937a50de7441ca5b9ba49a4198',1,'Map.cpp']]],
  ['colour_5fkey_5fdrink_32425',['COLOUR_KEY_DRINK',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa19b4b556bfefcabd7035a461a5347e1b',1,'Map.cpp']]],
  ['colour_5fkey_5ffirst_5faid_32426',['COLOUR_KEY_FIRST_AID',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa7b269bb37873744c217ddb8f0342f98a',1,'Map.cpp']]],
  ['colour_5fkey_5ffood_32427',['COLOUR_KEY_FOOD',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa3c8f5553e23093335f83541741d0c6d9',1,'Map.cpp']]],
  ['colour_5fkey_5fkiosk_32428',['COLOUR_KEY_KIOSK',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa2ed71dabf1865736a328e2f60fd35dc0',1,'Map.cpp']]],
  ['colour_5fkey_5fride_32429',['COLOUR_KEY_RIDE',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa8a5c41d46ab9c9b95139dc4a8cbca956',1,'Map.cpp']]],
  ['colour_5fkey_5fsouvenir_32430',['COLOUR_KEY_SOUVENIR',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa1b00b1143381fc968702aff5fab09243',1,'Map.cpp']]],
  ['colour_5fkey_5ftoilets_32431',['COLOUR_KEY_TOILETS',['../ui_2windows_2_map_8cpp.html#ad04e28e1e9d0978443b2354185469e0fa479f0f8595ca1225b67831550a1792a4',1,'Map.cpp']]],
  ['colour_5flight_5fblue_32432',['COLOUR_LIGHT_BLUE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3aed02776deb32f401e920554626bf46da',1,'Colour.h']]],
  ['colour_5flight_5fbrown_32433',['COLOUR_LIGHT_BROWN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a059be63230bbcb8f88ee033d7d642115',1,'Colour.h']]],
  ['colour_5flight_5forange_32434',['COLOUR_LIGHT_ORANGE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ac36a0872c6481c36e57774a807fad304',1,'Colour.h']]],
  ['colour_5flight_5fpink_32435',['COLOUR_LIGHT_PINK',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a0759dc79756e10d3b97d3d7459d2e6cb',1,'Colour.h']]],
  ['colour_5flight_5fpurple_32436',['COLOUR_LIGHT_PURPLE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a5138c0344bb50ea235fb2657f4bdd8e7',1,'Colour.h']]],
  ['colour_5flight_5fwater_32437',['COLOUR_LIGHT_WATER',['../_colour_8h.html#ae8a3b6a5d0d3244ed73924ab2421a0d0a61d3f9388770859252547b19d1cdca8c',1,'Colour.h']]],
  ['colour_5fmethod_5fansi_32438',['COLOUR_METHOD_ANSI',['../test_2testpaint_2_main_8cpp.html#a785e4ed87b2d1317cc57e02f5f2f96d7aa98496cf233d9118c470822a1c60448c',1,'main.cpp']]],
  ['colour_5fmethod_5fnone_32439',['COLOUR_METHOD_NONE',['../test_2testpaint_2_main_8cpp.html#a785e4ed87b2d1317cc57e02f5f2f96d7aa4b84f882ab8360ab7b99abbe1f7402e',1,'main.cpp']]],
  ['colour_5fmethod_5fwindows_32440',['COLOUR_METHOD_WINDOWS',['../test_2testpaint_2_main_8cpp.html#a785e4ed87b2d1317cc57e02f5f2f96d7aa03e380aa39acfcf49485c23db4c9a3d',1,'main.cpp']]],
  ['colour_5fmoss_5fgreen_32441',['COLOUR_MOSS_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a1a03b8ecc5d5f2ea40185caefb42c192',1,'Colour.h']]],
  ['colour_5folive_5fgreen_32442',['COLOUR_OLIVE_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a6b77e4d01cf34baed6658a6ea3c80aee',1,'Colour.h']]],
  ['colour_5fsalmon_5fpink_32443',['COLOUR_SALMON_PINK',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3ad031d22ae9ef7dc392a22e090746c172',1,'Colour.h']]],
  ['colour_5fsaturated_5fbrown_32444',['COLOUR_SATURATED_BROWN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a939a4aecff69ffaec699cc0e99e7bb07',1,'Colour.h']]],
  ['colour_5fsaturated_5fgreen_32445',['COLOUR_SATURATED_GREEN',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a369f4e15c0514b2382b04f385d5fd3e3',1,'Colour.h']]],
  ['colour_5fsaturated_5fred_32446',['COLOUR_SATURATED_RED',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3af04b1e5f7b7649f7b04551c4f0f31831',1,'Colour.h']]],
  ['colour_5fteal_32447',['COLOUR_TEAL',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a7e7c4c188b34486ac748a63722869cde',1,'Colour.h']]],
  ['colour_5fwhite_32448',['COLOUR_WHITE',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a99430b242fe57c913fe4fb8b7e430fc0',1,'Colour.h']]],
  ['colour_5fyellow_32449',['COLOUR_YELLOW',['../_colour_8h.html#af3520ff6d43011872bab77edd27d4de3a3c9764a551f845d7e34fff950c02ed28',1,'Colour.h']]],
  ['config_32450',['CONFIG',['../namespace_open_r_c_t2.html#a22b7f516bd872b4e365d64dcd94d86aba73e99d350a4aa6f1a5af04ec29173f73',1,'OpenRCT2::CONFIG()'],['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0a73e99d350a4aa6f1a5af04ec29173f73',1,'OpenRCT2::CONFIG()']]],
  ['config_5fkeyboard_32451',['CONFIG_KEYBOARD',['../namespace_open_r_c_t2.html#abdc11a255eac9d7657365cd3455bece0a79a587ee55fd7469e72321e0e1d6db88',1,'OpenRCT2']]],
  ['console_5finput_5fhistory_5fnext_32452',['CONSOLE_INPUT_HISTORY_NEXT',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743a8752f7f850d8b82763455abbe5e95f48',1,'InteractiveConsole.h']]],
  ['console_5finput_5fhistory_5fprevious_32453',['CONSOLE_INPUT_HISTORY_PREVIOUS',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743a19753b370753cedf45647c89de7c2d17',1,'InteractiveConsole.h']]],
  ['console_5finput_5fline_5fclear_32454',['CONSOLE_INPUT_LINE_CLEAR',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743a1e16d7561727ab99e9707a3a37b3952f',1,'InteractiveConsole.h']]],
  ['console_5finput_5fline_5fexecute_32455',['CONSOLE_INPUT_LINE_EXECUTE',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743a1f7a5f00701cbf196e59e4f7b483e837',1,'InteractiveConsole.h']]],
  ['console_5finput_5fnone_32456',['CONSOLE_INPUT_NONE',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743ac2a823baec5a510946afad487563831e',1,'InteractiveConsole.h']]],
  ['console_5finput_5fscroll_5fnext_32457',['CONSOLE_INPUT_SCROLL_NEXT',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743a02ec50f35e0f9f89f4535cd13aed61a0',1,'InteractiveConsole.h']]],
  ['console_5finput_5fscroll_5fprevious_32458',['CONSOLE_INPUT_SCROLL_PREVIOUS',['../_interactive_console_8h.html#accaeaf0cac040c477cde8c293a5cb743aae4f2eb310967650ca454f42a934b3a3',1,'InteractiveConsole.h']]],
  ['continuous_5fchasers_32459',['CONTINUOUS_CHASERS',['../_fountain_8cpp.html#ac6d8e95bea73aef6632596a817f1c9e0a6087f8ca7c5e4a6d048408a6f924309e',1,'Fountain.cpp']]],
  ['copy_5fcolour_5f1_32460',['COPY_COLOUR_1',['../_r_c_t1_8h.html#a737c1bdca78a4eec48fa026fcca3dd9baad26253b89d1bd7f7db78efd157e22e2',1,'RCT1.h']]],
  ['copy_5fcolour_5f2_32461',['COPY_COLOUR_2',['../_r_c_t1_8h.html#a737c1bdca78a4eec48fa026fcca3dd9bad44176017cc9325315f6fb86c5955a2b',1,'RCT1.h']]],
  ['corkscrew_5frc_5fcarriage_32462',['CORKSCREW_RC_CARRIAGE',['../_r_c_t1_8h.html#aeb8d583a31cccbb8cf9b452a8e298063acb82be6fa57fe42944f55d92dc7a8238',1,'RCT1.h']]],
  ['corkscrew_5frc_5ffront_32463',['CORKSCREW_RC_FRONT',['../_r_c_t1_8h.html#aeb8d583a31cccbb8cf9b452a8e298063ad2fa4800fef12a72d00c2fb5c7fb74b0',1,'RCT1.h']]],
  ['corner_5fbottom_32464',['CORNER_BOTTOM',['../_paint_8_surface_8cpp.html#afb24d298ddd4bc4ff61aa333f07a574aa6b69e27e19d02c7d0db9acb3de81d9c9',1,'Paint.Surface.cpp']]],
  ['corner_5fleft_32465',['CORNER_LEFT',['../_paint_8_surface_8cpp.html#afb24d298ddd4bc4ff61aa333f07a574aaa36e74cf1b3578e68a04b72717724ce4',1,'Paint.Surface.cpp']]],
  ['corner_5fright_32466',['CORNER_RIGHT',['../_paint_8_surface_8cpp.html#afb24d298ddd4bc4ff61aa333f07a574aae1e783bebec23008efb1213cc49ecf8b',1,'Paint.Surface.cpp']]],
  ['corner_5ftop_32467',['CORNER_TOP',['../_paint_8_surface_8cpp.html#afb24d298ddd4bc4ff61aa333f07a574aa4dd233426440ba4879b2f776d3befa45',1,'Paint.Surface.cpp']]],
  ['corrupt_32468',['Corrupt',['../_r_c_t12_8h.html#a59bff1292891d44436b9461101cda2b0a6e25aa27fcd893613fac13b0312fe36d',1,'Corrupt():&#160;RCT12.h'],['../_tile_element_8h.html#a85b2e6b80be25fcf3f8dec2a309a49f3a6e25aa27fcd893613fac13b0312fe36d',1,'Corrupt():&#160;TileElement.h']]],
  ['corruptclamp_32469',['CorruptClamp',['../_tile_modify_action_8hpp.html#abfab082d1064acc75e72820c9abd9780a000581c99204492d6bd4e623a71475a0',1,'TileModifyAction.hpp']]],
  ['costtobuyconstructionrights_32470',['CostToBuyConstructionRights',['../_scenario_set_setting_action_8hpp.html#ad51380bcc3e34b5c94cba8bbaa1b4b44a0ff8e07d2326171e016cb31d6ec5103a',1,'ScenarioSetSettingAction.hpp']]],
  ['costtobuyland_32471',['CostToBuyLand',['../_scenario_set_setting_action_8hpp.html#ad51380bcc3e34b5c94cba8bbaa1b4b44abcc02ae4818be02f0b02cb8caf36ab4b',1,'ScenarioSetSettingAction.hpp']]],
  ['cough1_32472',['Cough1',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca12f4e32ca93c4288d310a8ad55c51051',1,'audio.h']]],
  ['cough2_32473',['Cough2',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca1f1dc06e60e9c9a8883f3fb8926841da',1,'audio.h']]],
  ['cough3_32474',['Cough3',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bcaa48791d804ce455ca84873982554a33a',1,'audio.h']]],
  ['cough4_32475',['Cough4',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca4ba89e421fcec58842ad158ea1aa2715',1,'audio.h']]],
  ['count_32476',['Count',['../_banner_set_style_action_8hpp.html#a7d88841cf095a4fb5e8a7c396649d3f5ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;BannerSetStyleAction.hpp'],['../_land_buy_rights_action_8hpp.html#af2bef25320d2ad4b7ac65e8bb5ed403fae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;LandBuyRightsAction.hpp'],['../_land_set_rights_action_8hpp.html#ac7f448eab438e5296885f2bea9b7da16ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;LandSetRightsAction.hpp'],['../_network_modify_group_action_8hpp.html#a477c05f5d3fe0101726ea7e93d8f5f85ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;NetworkModifyGroupAction.hpp'],['../_network_modify_group_action_8hpp.html#a092502b840c7cf2c9379b470cfdd83fdae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;NetworkModifyGroupAction.hpp'],['../_park_set_parameter_action_8hpp.html#a8a8b8e901d3f1d0cec7e1f0ecaed3dceae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;ParkSetParameterAction.hpp'],['../_peep_pickup_action_8hpp.html#aaa88cb88c4c88520d83ac6ba0c72aba8ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;PeepPickupAction.hpp'],['../_scenario_set_setting_action_8hpp.html#ad51380bcc3e34b5c94cba8bbaa1b4b44ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;ScenarioSetSettingAction.hpp'],['../_tile_modify_action_8hpp.html#abfab082d1064acc75e72820c9abd9780ae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;TileModifyAction.hpp'],['../_cheats_8h.html#a89b6419984cb46d3ae8f5bfe41c742aeae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;Cheats.h'],['../_footpath_8h.html#a5307eb391f46733d0a78d3f8018ed95dae93f994f01c537c4e2f7d8528c3eb5e9',1,'Count():&#160;Footpath.h']]],
  ['crash_32477',['Crash',['../audio_8h.html#ae5bd36a2c4997c7b627ab14cd465a8bca275c74140224026380b29b2412d25121',1,'audio.h']]],
  ['create_5fcrossing_5fmode_5fnone_32478',['CREATE_CROSSING_MODE_NONE',['../_map_8h.html#aaaada5eefb2d5609f08676feda4171cdacae612355817938dd77434e7dd19e122',1,'Map.h']]],
  ['create_5fcrossing_5fmode_5fpath_5fover_5ftrack_32479',['CREATE_CROSSING_MODE_PATH_OVER_TRACK',['../_map_8h.html#aaaada5eefb2d5609f08676feda4171cdaebb944c9dc8af688bd2d4332d16c5c43',1,'Map.h']]],
  ['create_5fcrossing_5fmode_5ftrack_5fover_5fpath_32480',['CREATE_CROSSING_MODE_TRACK_OVER_PATH',['../_map_8h.html#aaaada5eefb2d5609f08676feda4171cda2d59c704aed923a17c19534c50d0d14b',1,'Map.h']]],
  ['createducks_32481',['CreateDucks',['../_cheats_8h.html#a89b6419984cb46d3ae8f5bfe41c742aeab55ef1d54f229a70647783b5d4f7e479',1,'Cheats.h']]],
  ['csg_32482',['CSG',['../_drawing_8h.html#a7081205853e3bac08dd5677e1aa4a9d5addf86b0b94e34566c486fd076b9686a6',1,'Drawing.h']]],
  ['ctrl_5fa_32483',['CTRL_A',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a38f396b34c4dd205a862f0b9bc2fda18',1,'linenoise']]],
  ['ctrl_5fb_32484',['CTRL_B',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a3484cfa59c025a4e991712315c6207a8',1,'linenoise']]],
  ['ctrl_5fc_32485',['CTRL_C',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184ac7fd9fd674c4745f23649a34125425d0',1,'linenoise']]],
  ['ctrl_5fd_32486',['CTRL_D',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184af8fed46941f0b8ca3d1b40f85d7782fa',1,'linenoise']]],
  ['ctrl_5fe_32487',['CTRL_E',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a9fb451bf14a29cb7309a56224ab3d882',1,'linenoise']]],
  ['ctrl_5ff_32488',['CTRL_F',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a6e572b385df7ab120b5bde7ebfe4102c',1,'linenoise']]],
  ['ctrl_5fh_32489',['CTRL_H',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a29f21bf31b56866d55dd96fab4e3275c',1,'linenoise']]],
  ['ctrl_5fk_32490',['CTRL_K',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a718a8259c4f329f3aca9047d63193a1a',1,'linenoise']]],
  ['ctrl_5fl_32491',['CTRL_L',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a2253ad9e598fbeea544429c901f381a3',1,'linenoise']]],
  ['ctrl_5fn_32492',['CTRL_N',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a272af9307e2f50057820c844b70c107a',1,'linenoise']]],
  ['ctrl_5fp_32493',['CTRL_P',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a0ac174e1ad1cc8d3465ca6d01c894374',1,'linenoise']]],
  ['ctrl_5ft_32494',['CTRL_T',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a75fd293432e84855807696a949557e6b',1,'linenoise']]],
  ['ctrl_5fu_32495',['CTRL_U',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a431ecf41e99540f17de80785ae8dedba',1,'linenoise']]],
  ['ctrl_5fw_32496',['CTRL_W',['../namespacelinenoise.html#af862ccf21a7d5fd298fffcfe084e1184a9945f2c1450b9dfc2b70685e936addbd',1,'linenoise']]],
  ['currency_5fcustom_32497',['CURRENCY_CUSTOM',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8ae1954b9ac053c721e3749778a4937da7',1,'Currency.h']]],
  ['currency_5fczech_5fkoruna_32498',['CURRENCY_CZECH_KORUNA',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a488ded7b7d4247b55e655f80b3be381d',1,'Currency.h']]],
  ['currency_5fdeutschmark_32499',['CURRENCY_DEUTSCHMARK',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a46a3fcfec8087c75bee4f9f102d6a6b7',1,'Currency.h']]],
  ['currency_5fdollars_32500',['CURRENCY_DOLLARS',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a1a645e493361d34e05cb595e0d855be9',1,'Currency.h']]],
  ['currency_5fend_32501',['CURRENCY_END',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a9182d67662a33b0eb17beee97ec6d611',1,'Currency.h']]],
  ['currency_5feuros_32502',['CURRENCY_EUROS',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a36a78fb25606de13ae9d0c0e56d6bfc2',1,'Currency.h']]],
  ['currency_5fforint_32503',['CURRENCY_FORINT',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a437b04f46ec998040661b22a65dcb06a',1,'Currency.h']]],
  ['currency_5ffranc_32504',['CURRENCY_FRANC',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a5eb947108599a8acbb28eef60b8e8080',1,'Currency.h']]],
  ['currency_5fguilders_32505',['CURRENCY_GUILDERS',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8af146424a34e4995131e6c522d8004e18',1,'Currency.h']]],
  ['currency_5fhkd_32506',['CURRENCY_HKD',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8aed2ae827e8bd51a466759e6bc09dcd4e',1,'Currency.h']]],
  ['currency_5fkrona_32507',['CURRENCY_KRONA',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a8c45daa52e8c29384b99d07b82b05717',1,'Currency.h']]],
  ['currency_5flira_32508',['CURRENCY_LIRA',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a81df88e6c7833025119b0818e4d31f12',1,'Currency.h']]],
  ['currency_5fpeseta_32509',['CURRENCY_PESETA',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a3939e1b340b966adf9c79997dd178ec1',1,'Currency.h']]],
  ['currency_5fpounds_32510',['CURRENCY_POUNDS',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a9b165c0f20c6adbe82f40c14dd3ac4b7',1,'Currency.h']]],
  ['currency_5fprefix_32511',['CURRENCY_PREFIX',['../_currency_8h.html#a5abba9a6c1504257b3a15f7e7288bcb8a766c1905a5f22c42b5633cd8a0e86edf',1,'Currency.h']]],
  ['currency_5frouble_32512',['CURRENCY_ROUBLE',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8ac17a88c367980f4a62c789e1ef0d8e39',1,'Currency.h']]],
  ['currency_5fsuffix_32513',['CURRENCY_SUFFIX',['../_currency_8h.html#a5abba9a6c1504257b3a15f7e7288bcb8adef55d82f11f015da7e620e687a69643',1,'Currency.h']]],
  ['currency_5ftwd_32514',['CURRENCY_TWD',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8aacb66f62aeb6582be93a3ab31597e255',1,'Currency.h']]],
  ['currency_5fwon_32515',['CURRENCY_WON',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a608da8317b3451adf2480fedc7bc0df4',1,'Currency.h']]],
  ['currency_5fyen_32516',['CURRENCY_YEN',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8ab1622ca7b7bdbb38b5446be57592e437',1,'Currency.h']]],
  ['currency_5fyuan_32517',['CURRENCY_YUAN',['../_currency_8h.html#a95efe1f0ceadac6871be1177a32dd7b8a48ceb044ce65f620c4a96c926099d7da',1,'Currency.h']]],
  ['cursor_5farrow_32518',['CURSOR_ARROW',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a8ecb986da99c3c8f74789613a9858492',1,'Cursors.h']]],
  ['cursor_5fbench_5fdown_32519',['CURSOR_BENCH_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a94aeac938cfa4d96779609f9b907a887',1,'Cursors.h']]],
  ['cursor_5fbin_5fdown_32520',['CURSOR_BIN_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a46e83836ccca38d9f340603e7f379cbe',1,'Cursors.h']]],
  ['cursor_5fblank_32521',['CURSOR_BLANK',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68af25200de18880bb2fdb6de70c85b777b',1,'Cursors.h']]],
  ['cursor_5fchanged_32522',['CURSOR_CHANGED',['../_context_8h.html#a7ff5f2dff38e7639981794c43dc9167bad3107657af5f5619645e6b28723c8a24',1,'Context.h']]],
  ['cursor_5fcount_32523',['CURSOR_COUNT',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a29af30643057471c14a78320b6cf3a6a',1,'Cursors.h']]],
  ['cursor_5fcross_5fhair_32524',['CURSOR_CROSS_HAIR',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a51835ef315f532a3bed24eb478cecb0f',1,'Cursors.h']]],
  ['cursor_5fdiagonal_5farrows_32525',['CURSOR_DIAGONAL_ARROWS',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a6ecb1a97f84d332eabd217f6cdd03357',1,'Cursors.h']]],
  ['cursor_5fdig_5fdown_32526',['CURSOR_DIG_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68aaea4da84726ac99c56e433245d11da3b',1,'Cursors.h']]],
  ['cursor_5fdown_32527',['CURSOR_DOWN',['../_context_8h.html#a7ff5f2dff38e7639981794c43dc9167ba9bd2c7c2e70700c98eb7ed895ce47f82',1,'Context.h']]],
  ['cursor_5fentrance_5fdown_32528',['CURSOR_ENTRANCE_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a69eaab6e561a28511a6b8185dcc83dbb',1,'Cursors.h']]],
  ['cursor_5ffence_5fdown_32529',['CURSOR_FENCE_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68afbc55601d80b73b8fdeccae9d8cde758',1,'Cursors.h']]],
  ['cursor_5fflower_5fdown_32530',['CURSOR_FLOWER_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68ab66efdef00b87b0864267a5db621c9f9',1,'Cursors.h']]],
  ['cursor_5ffountain_5fdown_32531',['CURSOR_FOUNTAIN_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68ac55832394f2020b01af27367189715fd',1,'Cursors.h']]],
  ['cursor_5fhand_5fclosed_32532',['CURSOR_HAND_CLOSED',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a036c29392778748e038a3e6d43c3fd97',1,'Cursors.h']]],
  ['cursor_5fhand_5fopen_32533',['CURSOR_HAND_OPEN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68ab52679a17d0f598a370fc3d2401424a0',1,'Cursors.h']]],
  ['cursor_5fhand_5fpoint_32534',['CURSOR_HAND_POINT',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a85b82f1ed13c90b0f0d490913206f8ee',1,'Cursors.h']]],
  ['cursor_5fhouse_5fdown_32535',['CURSOR_HOUSE_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a2d484a8da57ce593a1c9cd171345f146',1,'Cursors.h']]],
  ['cursor_5flamppost_5fdown_32536',['CURSOR_LAMPPOST_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68aa2977c2a3781ef3a3ab73051f3d918a3',1,'Cursors.h']]],
  ['cursor_5fpaint_5fdown_32537',['CURSOR_PAINT_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a30c4ec787ff06f1a4c0345afdd35ee95',1,'Cursors.h']]],
  ['cursor_5fpath_5fdown_32538',['CURSOR_PATH_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a894f5c6b64aae6f4f27536855e9d083f',1,'Cursors.h']]],
  ['cursor_5fpicker_32539',['CURSOR_PICKER',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68aae2f9417499dabec6e14f51a13b43b84',1,'Cursors.h']]],
  ['cursor_5fpressed_32540',['CURSOR_PRESSED',['../_context_8h.html#a7ff5f2dff38e7639981794c43dc9167ba54377b6eb191342572d02095756344aa',1,'Context.h']]],
  ['cursor_5freleased_32541',['CURSOR_RELEASED',['../_context_8h.html#a7ff5f2dff38e7639981794c43dc9167ba463cf643cb3503a138a736fd33ef3885',1,'Context.h']]],
  ['cursor_5fstatue_5fdown_32542',['CURSOR_STATUE_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a257cd6a017443a2fe9a7eb7c762c527b',1,'Cursors.h']]],
  ['cursor_5ftree_5fdown_32543',['CURSOR_TREE_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a3e6935ec04d01679cedf5aca8b63cc61',1,'Cursors.h']]],
  ['cursor_5fundefined_32544',['CURSOR_UNDEFINED',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a5f1143bcb7df6705cb5777cd6cd2f484',1,'Cursors.h']]],
  ['cursor_5fup_32545',['CURSOR_UP',['../_context_8h.html#a7ff5f2dff38e7639981794c43dc9167bab0c4058c3097d23fce049b33970cd564',1,'Context.h']]],
  ['cursor_5fup_5farrow_32546',['CURSOR_UP_ARROW',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a58c4e5ed5d8dd22c2bf2127f97f5c0be',1,'Cursors.h']]],
  ['cursor_5fup_5fdown_5farrow_32547',['CURSOR_UP_DOWN_ARROW',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a2c81efb8ed48f7d070badaaa323ed6bb',1,'Cursors.h']]],
  ['cursor_5fvolcano_5fdown_32548',['CURSOR_VOLCANO_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a7d22ce318d062da5544c8d176953c379',1,'Cursors.h']]],
  ['cursor_5fwalk_5fdown_32549',['CURSOR_WALK_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a0f0524dfdb8d14231c0c5ccbdadfa148',1,'Cursors.h']]],
  ['cursor_5fwater_5fdown_32550',['CURSOR_WATER_DOWN',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68a6f3854313621fadb773bcbb115f572d3',1,'Cursors.h']]],
  ['cursor_5fzzz_32551',['CURSOR_ZZZ',['../_cursors_8h.html#ad04d49c9ddadc8daf75982f8edf1de68ac463e8b3ab949a15d4f333e143ed2184',1,'Cursors.h']]],
  ['cyclic_5fsquares_32552',['CYCLIC_SQUARES',['../_fountain_8cpp.html#ac6d8e95bea73aef6632596a817f1c9e0a164aae6e5651e7669daa2ea74f82a59d',1,'Fountain.cpp']]]
];
