var searchData=
[
  ['_5f_5fpad0_5f_5f_28053',['__pad0__',['../bin_2data_2language_2ar-_e_g_8txt.html#aa7bf5904897432657d32676811c37adc',1,'__pad0__():&#160;ar-EG.txt'],['../bpb_8sv6_8txt.html#a08950529aa9d277a1dc47c676fa17c02',1,'__pad0__():&#160;bpb.sv6.txt']]],
  ['_5f_5fpad10_5f_5f_28054',['__pad10__',['../bin_2data_2language_2en-_g_b_8txt.html#a2c8c884d3a20ae971f89296f62ae042b',1,'en-GB.txt']]],
  ['_5f_5fpad11_5f_5f_28055',['__pad11__',['../bin_2data_2language_2en-_g_b_8txt.html#ab4d7b1565034865a1bd8cf4c3f7f41e4',1,'en-GB.txt']]],
  ['_5f_5fpad12_5f_5f_28056',['__pad12__',['../bin_2data_2language_2en-_u_s_8txt.html#ad2fff81851c657d24a39cf4dde1d265b',1,'en-US.txt']]],
  ['_5f_5fpad13_5f_5f_28057',['__pad13__',['../bin_2data_2language_2es-_e_s_8txt.html#aec8519477c5482059dd501864e7c7e8d',1,'es-ES.txt']]],
  ['_5f_5fpad14_5f_5f_28058',['__pad14__',['../bin_2data_2language_2es-_e_s_8txt.html#a624a6a308504e34e6ce5dd4511f4bd49',1,'es-ES.txt']]],
  ['_5f_5fpad15_5f_5f_28059',['__pad15__',['../bin_2data_2language_2fi-_f_i_8txt.html#ab1b48fe06e8178c3f7bbf3718793d577',1,'fi-FI.txt']]],
  ['_5f_5fpad16_5f_5f_28060',['__pad16__',['../bin_2data_2language_2fi-_f_i_8txt.html#a82462a0adf7033d903c969ef22ad9f8a',1,'fi-FI.txt']]],
  ['_5f_5fpad17_5f_5f_28061',['__pad17__',['../bin_2data_2language_2fr-_f_r_8txt.html#ad5cc8a2af606b6df3bad167409d6b834',1,'fr-FR.txt']]],
  ['_5f_5fpad18_5f_5f_28062',['__pad18__',['../bin_2data_2language_2hu-_h_u_8txt.html#a1aad729c8ce95217118ce4cfa367aad8',1,'hu-HU.txt']]],
  ['_5f_5fpad19_5f_5f_28063',['__pad19__',['../bin_2data_2language_2hu-_h_u_8txt.html#a40487de982a52e3480d97c5d23bbc155',1,'hu-HU.txt']]],
  ['_5f_5fpad1_5f_5f_28064',['__pad1__',['../bin_2data_2language_2ar-_e_g_8txt.html#a68a8f454c898db4ecca91c3564b10a23',1,'ar-EG.txt']]],
  ['_5f_5fpad20_5f_5f_28065',['__pad20__',['../bin_2data_2language_2it-_i_t_8txt.html#a3d49d21c778215d8ad7c933983a7b97d',1,'it-IT.txt']]],
  ['_5f_5fpad21_5f_5f_28066',['__pad21__',['../bin_2data_2language_2it-_i_t_8txt.html#ac341dce4226cca60ab59a61416cca6d2',1,'it-IT.txt']]],
  ['_5f_5fpad22_5f_5f_28067',['__pad22__',['../bin_2data_2language_2ja-_j_p_8txt.html#a8d5a4f9c47353b0fd3e835171678dd0a',1,'ja-JP.txt']]],
  ['_5f_5fpad23_5f_5f_28068',['__pad23__',['../bin_2data_2language_2ja-_j_p_8txt.html#a2f67f07adff5bd353071813b432248ce',1,'ja-JP.txt']]],
  ['_5f_5fpad24_5f_5f_28069',['__pad24__',['../bin_2data_2language_2ko-_k_r_8txt.html#a0c161835e2b69e300b7c26697c73e620',1,'ko-KR.txt']]],
  ['_5f_5fpad25_5f_5f_28070',['__pad25__',['../bin_2data_2language_2ko-_k_r_8txt.html#a8e52e37b15fec9e99e296f49bf147855',1,'ko-KR.txt']]],
  ['_5f_5fpad26_5f_5f_28071',['__pad26__',['../bin_2data_2language_2nb-_n_o_8txt.html#af1926f860d6e9662b82de238768038ea',1,'nb-NO.txt']]],
  ['_5f_5fpad27_5f_5f_28072',['__pad27__',['../bin_2data_2language_2nb-_n_o_8txt.html#adcce20f4a9248222b7e7f1d8ac9b8da4',1,'nb-NO.txt']]],
  ['_5f_5fpad28_5f_5f_28073',['__pad28__',['../bin_2data_2language_2nl-_n_l_8txt.html#a6c03e15f9eace438499cbdf20844f085',1,'nl-NL.txt']]],
  ['_5f_5fpad29_5f_5f_28074',['__pad29__',['../bin_2data_2language_2nl-_n_l_8txt.html#a861729ff7fa0ed82e9f304316b0d202c',1,'nl-NL.txt']]],
  ['_5f_5fpad2_5f_5f_28075',['__pad2__',['../bin_2data_2language_2ca-_e_s_8txt.html#a80905b19bb1e51a03943acbd5066e4aa',1,'ca-ES.txt']]],
  ['_5f_5fpad30_5f_5f_28076',['__pad30__',['../bin_2data_2language_2pl-_p_l_8txt.html#a78a47a185124558089ae141b76aa9416',1,'pl-PL.txt']]],
  ['_5f_5fpad31_5f_5f_28077',['__pad31__',['../bin_2data_2language_2pl-_p_l_8txt.html#a56fa555d3171e435c8d9ceba8743e669',1,'pl-PL.txt']]],
  ['_5f_5fpad32_5f_5f_28078',['__pad32__',['../bin_2data_2language_2pt-_b_r_8txt.html#a011833ec4c2e1c6b7d9692557b167fac',1,'pt-BR.txt']]],
  ['_5f_5fpad33_5f_5f_28079',['__pad33__',['../bin_2data_2language_2pt-_b_r_8txt.html#a5084c1fd3cbff6bbc1c84b1c164427b4',1,'pt-BR.txt']]],
  ['_5f_5fpad34_5f_5f_28080',['__pad34__',['../bin_2data_2language_2ru-_r_u_8txt.html#aff2109e847052760e889841b26f8564b',1,'ru-RU.txt']]],
  ['_5f_5fpad35_5f_5f_28081',['__pad35__',['../bin_2data_2language_2ru-_r_u_8txt.html#ab1de370cba8490dfb0242924793d38ea',1,'ru-RU.txt']]],
  ['_5f_5fpad36_5f_5f_28082',['__pad36__',['../bin_2data_2language_2sv-_s_e_8txt.html#ae098c70fc0dd912418ab76cab5b69581',1,'sv-SE.txt']]],
  ['_5f_5fpad37_5f_5f_28083',['__pad37__',['../bin_2data_2language_2sv-_s_e_8txt.html#a7202c31b96c8a500ce95d3d677daf423',1,'sv-SE.txt']]],
  ['_5f_5fpad38_5f_5f_28084',['__pad38__',['../bin_2data_2language_2tr-_t_r_8txt.html#a1185556233cc627b64b900b9b7be19e0',1,'tr-TR.txt']]],
  ['_5f_5fpad39_5f_5f_28085',['__pad39__',['../bin_2data_2language_2tr-_t_r_8txt.html#a91c68c5a121fcf904d38c075cb45a011',1,'tr-TR.txt']]],
  ['_5f_5fpad3_5f_5f_28086',['__pad3__',['../bin_2data_2language_2ca-_e_s_8txt.html#ab868a30de6c099e69e69221ec8cd4004',1,'ca-ES.txt']]],
  ['_5f_5fpad40_5f_5f_28087',['__pad40__',['../bin_2data_2language_2zh-_c_n_8txt.html#a6117213c8b44162aa1dc62638cf1cfb3',1,'zh-CN.txt']]],
  ['_5f_5fpad41_5f_5f_28088',['__pad41__',['../bin_2data_2language_2zh-_c_n_8txt.html#a39a9bf05f97ee70d6b2f64ee3da62600',1,'zh-CN.txt']]],
  ['_5f_5fpad42_5f_5f_28089',['__pad42__',['../bin_2data_2language_2zh-_c_n_8txt.html#a5d6012e8734f110a7d2c5c484d3090d5',1,'zh-CN.txt']]],
  ['_5f_5fpad43_5f_5f_28090',['__pad43__',['../bin_2data_2language_2zh-_t_w_8txt.html#a765a60df64694b08fc94e6884996b577',1,'zh-TW.txt']]],
  ['_5f_5fpad44_5f_5f_28091',['__pad44__',['../bin_2data_2language_2zh-_t_w_8txt.html#ae6a53e58a816068dd45ca4ad01443d35',1,'zh-TW.txt']]],
  ['_5f_5fpad45_5f_5f_28092',['__pad45__',['../bin_2data_2language_2zh-_t_w_8txt.html#a9890f9a4ffb9b89dbb193426a0c23608',1,'zh-TW.txt']]],
  ['_5f_5fpad46_5f_5f_28093',['__pad46__',['../data_2language_2ar-_e_g_8txt.html#a6dd230be6ff4ec6a7d400c2a789443d5',1,'ar-EG.txt']]],
  ['_5f_5fpad47_5f_5f_28094',['__pad47__',['../data_2language_2ar-_e_g_8txt.html#aa9e80fec75f31d93fd4e657b22264740',1,'ar-EG.txt']]],
  ['_5f_5fpad48_5f_5f_28095',['__pad48__',['../data_2language_2ca-_e_s_8txt.html#a7e71dfff4cf768e3212649fd1b23a6e4',1,'ca-ES.txt']]],
  ['_5f_5fpad49_5f_5f_28096',['__pad49__',['../data_2language_2ca-_e_s_8txt.html#a083e521ebad7f9587aa7d4e5c60ac0ce',1,'ca-ES.txt']]],
  ['_5f_5fpad4_5f_5f_28097',['__pad4__',['../bin_2data_2language_2cs-_c_z_8txt.html#ad3e916f2a1aee80751811e0a38f77a3a',1,'cs-CZ.txt']]],
  ['_5f_5fpad50_5f_5f_28098',['__pad50__',['../data_2language_2cs-_c_z_8txt.html#a08f4c1601a55c3732a545df652e021a4',1,'cs-CZ.txt']]],
  ['_5f_5fpad51_5f_5f_28099',['__pad51__',['../data_2language_2cs-_c_z_8txt.html#a216c8e2bd85d4243c4253b519872ab58',1,'cs-CZ.txt']]],
  ['_5f_5fpad52_5f_5f_28100',['__pad52__',['../data_2language_2da-_d_k_8txt.html#aa4cf1c91ca0aebe19150902f4d7da0c9',1,'da-DK.txt']]],
  ['_5f_5fpad53_5f_5f_28101',['__pad53__',['../data_2language_2da-_d_k_8txt.html#a3ae0028f34a76cf249fc2ccf2f7b5037',1,'da-DK.txt']]],
  ['_5f_5fpad54_5f_5f_28102',['__pad54__',['../data_2language_2de-_d_e_8txt.html#a0edaafd0227e832d9285e971cf48a2b2',1,'de-DE.txt']]],
  ['_5f_5fpad55_5f_5f_28103',['__pad55__',['../data_2language_2de-_d_e_8txt.html#a53f6c19c344307155a5775cf7ba07bde',1,'de-DE.txt']]],
  ['_5f_5fpad56_5f_5f_28104',['__pad56__',['../data_2language_2en-_g_b_8txt.html#af105c52f7955ec6e6b5bf749c4b6e76f',1,'en-GB.txt']]],
  ['_5f_5fpad57_5f_5f_28105',['__pad57__',['../data_2language_2en-_g_b_8txt.html#adfca4207036da382be925817c4ae0880',1,'en-GB.txt']]],
  ['_5f_5fpad58_5f_5f_28106',['__pad58__',['../data_2language_2en-_u_s_8txt.html#a2df07abf86cf6d79cd9cf6223d9ec2a9',1,'en-US.txt']]],
  ['_5f_5fpad59_5f_5f_28107',['__pad59__',['../data_2language_2es-_e_s_8txt.html#a6fb5e20082bcf82af8401ea31f9b6388',1,'es-ES.txt']]],
  ['_5f_5fpad5_5f_5f_28108',['__pad5__',['../bin_2data_2language_2cs-_c_z_8txt.html#aa752c68e6218e8e33e9503798a0b681d',1,'cs-CZ.txt']]],
  ['_5f_5fpad60_5f_5f_28109',['__pad60__',['../data_2language_2es-_e_s_8txt.html#a46d1cba8bbea58cb887c91e3de48ef7b',1,'es-ES.txt']]],
  ['_5f_5fpad61_5f_5f_28110',['__pad61__',['../data_2language_2fi-_f_i_8txt.html#ae547d984c84a8cc60b1e1c691603e80e',1,'fi-FI.txt']]],
  ['_5f_5fpad62_5f_5f_28111',['__pad62__',['../data_2language_2fi-_f_i_8txt.html#a08cbd018d4d90d8452e3f09fdf66c6d8',1,'fi-FI.txt']]],
  ['_5f_5fpad63_5f_5f_28112',['__pad63__',['../data_2language_2fr-_f_r_8txt.html#ae36e7f3b62f9dbce0e139917fe402ba9',1,'fr-FR.txt']]],
  ['_5f_5fpad64_5f_5f_28113',['__pad64__',['../data_2language_2hu-_h_u_8txt.html#a3926d7f24d16f50e5b1ba7bb21312c15',1,'hu-HU.txt']]],
  ['_5f_5fpad65_5f_5f_28114',['__pad65__',['../data_2language_2hu-_h_u_8txt.html#ad805902d85fc42b0f92764379030cb9f',1,'hu-HU.txt']]],
  ['_5f_5fpad66_5f_5f_28115',['__pad66__',['../data_2language_2it-_i_t_8txt.html#a02a449ebfbcfbbd2d01cab76b3f61007',1,'it-IT.txt']]],
  ['_5f_5fpad67_5f_5f_28116',['__pad67__',['../data_2language_2it-_i_t_8txt.html#a7062833d6204b540ca1568db33f77401',1,'it-IT.txt']]],
  ['_5f_5fpad68_5f_5f_28117',['__pad68__',['../data_2language_2ja-_j_p_8txt.html#a624ff0e7eedefb9269c0b5dd0e3338f7',1,'ja-JP.txt']]],
  ['_5f_5fpad69_5f_5f_28118',['__pad69__',['../data_2language_2ja-_j_p_8txt.html#afe1375805a0b833194c21f177f06eb9c',1,'ja-JP.txt']]],
  ['_5f_5fpad6_5f_5f_28119',['__pad6__',['../bin_2data_2language_2da-_d_k_8txt.html#ac4e42f69af5a0eaf292268b4129d2769',1,'da-DK.txt']]],
  ['_5f_5fpad70_5f_5f_28120',['__pad70__',['../data_2language_2ko-_k_r_8txt.html#aa392594389dd4b953ba6ab9985dea157',1,'ko-KR.txt']]],
  ['_5f_5fpad71_5f_5f_28121',['__pad71__',['../data_2language_2ko-_k_r_8txt.html#a8f65c893522c71196f831bfcc6004fc2',1,'ko-KR.txt']]],
  ['_5f_5fpad72_5f_5f_28122',['__pad72__',['../data_2language_2nb-_n_o_8txt.html#a6526a9285741d653615a7e2504f5d8c5',1,'nb-NO.txt']]],
  ['_5f_5fpad73_5f_5f_28123',['__pad73__',['../data_2language_2nb-_n_o_8txt.html#aad7a75f960e3d76722bcfb7d3971584e',1,'nb-NO.txt']]],
  ['_5f_5fpad74_5f_5f_28124',['__pad74__',['../data_2language_2nl-_n_l_8txt.html#a395f11351e3c75909196a151bc3dc64f',1,'nl-NL.txt']]],
  ['_5f_5fpad75_5f_5f_28125',['__pad75__',['../data_2language_2nl-_n_l_8txt.html#a12419388b7865eb277542a4e3dbdf671',1,'nl-NL.txt']]],
  ['_5f_5fpad76_5f_5f_28126',['__pad76__',['../data_2language_2pl-_p_l_8txt.html#a27cc0f77a0f9ab105baed26cbde11db9',1,'pl-PL.txt']]],
  ['_5f_5fpad77_5f_5f_28127',['__pad77__',['../data_2language_2pl-_p_l_8txt.html#a598629df6d1bd48fd5c4480eee3ef5da',1,'pl-PL.txt']]],
  ['_5f_5fpad78_5f_5f_28128',['__pad78__',['../data_2language_2pt-_b_r_8txt.html#a5cbfbfa99ee03a48548d18ca21fb38e2',1,'pt-BR.txt']]],
  ['_5f_5fpad79_5f_5f_28129',['__pad79__',['../data_2language_2pt-_b_r_8txt.html#a3e521e5a4410740232ed88c61aefb5e8',1,'pt-BR.txt']]],
  ['_5f_5fpad7_5f_5f_28130',['__pad7__',['../bin_2data_2language_2da-_d_k_8txt.html#aa68af8585b9dcc7ad87f4da6e2d5e448',1,'da-DK.txt']]],
  ['_5f_5fpad80_5f_5f_28131',['__pad80__',['../data_2language_2ru-_r_u_8txt.html#a6ab38e463264da8db44eaa2a746da8f8',1,'ru-RU.txt']]],
  ['_5f_5fpad81_5f_5f_28132',['__pad81__',['../data_2language_2ru-_r_u_8txt.html#a268ac82ba83f3df5d69afc1777bc4944',1,'ru-RU.txt']]],
  ['_5f_5fpad82_5f_5f_28133',['__pad82__',['../data_2language_2sv-_s_e_8txt.html#a5ab659e6bdc28338c27b253f855b5463',1,'sv-SE.txt']]],
  ['_5f_5fpad83_5f_5f_28134',['__pad83__',['../data_2language_2sv-_s_e_8txt.html#a442c92a29f008c3ecb6a418cea409a13',1,'sv-SE.txt']]],
  ['_5f_5fpad84_5f_5f_28135',['__pad84__',['../data_2language_2tr-_t_r_8txt.html#ab4b6efc591d6c6a8c85b6e1c8e9e2c81',1,'tr-TR.txt']]],
  ['_5f_5fpad85_5f_5f_28136',['__pad85__',['../data_2language_2tr-_t_r_8txt.html#ac4cf6d8a950badb5f1df662ee6adccef',1,'tr-TR.txt']]],
  ['_5f_5fpad86_5f_5f_28137',['__pad86__',['../data_2language_2zh-_c_n_8txt.html#ab7109381008fea39fd5e631f963ca745',1,'zh-CN.txt']]],
  ['_5f_5fpad87_5f_5f_28138',['__pad87__',['../data_2language_2zh-_c_n_8txt.html#a7388f0fe48ccd9aa7a98daeef373c3a3',1,'zh-CN.txt']]],
  ['_5f_5fpad88_5f_5f_28139',['__pad88__',['../data_2language_2zh-_c_n_8txt.html#a298e41e07e8a41f8a0a4c3d2d12816d3',1,'zh-CN.txt']]],
  ['_5f_5fpad89_5f_5f_28140',['__pad89__',['../data_2language_2zh-_t_w_8txt.html#afc4c1e995d5a395a9ee62f431f31bdd0',1,'zh-TW.txt']]],
  ['_5f_5fpad8_5f_5f_28141',['__pad8__',['../bin_2data_2language_2de-_d_e_8txt.html#a531c445160e7eef6e5ba47dda7ac172d',1,'de-DE.txt']]],
  ['_5f_5fpad90_5f_5f_28142',['__pad90__',['../data_2language_2zh-_t_w_8txt.html#a2169334d42aba589539b500b53e2a1ef',1,'zh-TW.txt']]],
  ['_5f_5fpad91_5f_5f_28143',['__pad91__',['../data_2language_2zh-_t_w_8txt.html#a78233d9894dfbe9ac66a72b79c31dca7',1,'zh-TW.txt']]],
  ['_5f_5fpad92_5f_5f_28144',['__pad92__',['../changelog_8txt.html#a8fa94e9c68103499c583436b0d881d18',1,'changelog.txt']]],
  ['_5f_5fpad9_5f_5f_28145',['__pad9__',['../bin_2data_2language_2de-_d_e_8txt.html#aa79a739b2335250fdd019440bb5941a9',1,'de-DE.txt']]],
  ['_5fbits_28146',['_bits',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#abdd5ac9bd35e0332d94a9898ecbfa67a',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fbitsdpi_28147',['_bitsDPI',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a3d374c8b6e0592b915ff872b3cf071de',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fbitssize_28148',['_bitsSize',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#ae0bf2d32c31dc41d62c96d1034cf1daa',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fchallenge_28149',['_challenge',['../class_network.html#ad25a4a8384ae428016b366c3fa7fe900',1,'Network']]],
  ['_5fcurrentbrakespeed2_28150',['_currentBrakeSpeed2',['../ride_2_ride_8cpp.html#a74d5ad3da7ac1f48237ed19980677173',1,'_currentBrakeSpeed2():&#160;Ride.cpp'],['../_ride_8h.html#a74d5ad3da7ac1f48237ed19980677173',1,'_currentBrakeSpeed2():&#160;Ride.cpp']]],
  ['_5fcurrentfps_28151',['_currentFPS',['../namespace_open_r_c_t2_1_1_paint.html#a7afb1c6d126becd9953422e7d1ce01c0',1,'OpenRCT2::Paint']]],
  ['_5fcurrentrideindex_28152',['_currentRideIndex',['../ride_2_ride_8cpp.html#adf79978f99305a48290b6fe16b185459',1,'_currentRideIndex():&#160;Ride.cpp'],['../_ride_8h.html#adf79978f99305a48290b6fe16b185459',1,'_currentRideIndex():&#160;Ride.cpp']]],
  ['_5fcurrentseatrotationangle_28153',['_currentSeatRotationAngle',['../ride_2_ride_8cpp.html#a0c0da2f1ad76c66c660adb7de118820c',1,'_currentSeatRotationAngle():&#160;Ride.cpp'],['../_ride_8h.html#a0c0da2f1ad76c66c660adb7de118820c',1,'_currentSeatRotationAngle():&#160;Ride.cpp']]],
  ['_5fcurrenttrackalternative_28154',['_currentTrackAlternative',['../ride_2_ride_8cpp.html#a5f02b7a50e4968bb2c524c322ac69ab1',1,'_currentTrackAlternative():&#160;Ride.cpp'],['../_ride_8h.html#a5f02b7a50e4968bb2c524c322ac69ab1',1,'_currentTrackAlternative():&#160;Ride.cpp']]],
  ['_5fcurrenttrackbankend_28155',['_currentTrackBankEnd',['../ride_2_ride_8cpp.html#a354dcf8688bc3f0ee97b0c3d375deeae',1,'_currentTrackBankEnd():&#160;Ride.cpp'],['../_ride_8h.html#a354dcf8688bc3f0ee97b0c3d375deeae',1,'_currentTrackBankEnd():&#160;Ride.cpp']]],
  ['_5fcurrenttrackbegin_28156',['_currentTrackBegin',['../ride_2_ride_8cpp.html#aaaa0ae2f5a122424cafcb4af7f3d267e',1,'_currentTrackBegin():&#160;Ride.cpp'],['../_ride_8h.html#aaaa0ae2f5a122424cafcb4af7f3d267e',1,'_currentTrackBegin():&#160;Ride.cpp']]],
  ['_5fcurrenttrackcurve_28157',['_currentTrackCurve',['../ride_2_ride_8cpp.html#af53a04e83e86b2a8d53ce109b3187c2e',1,'_currentTrackCurve():&#160;Ride.cpp'],['../_ride_8h.html#af53a04e83e86b2a8d53ce109b3187c2e',1,'_currentTrackCurve():&#160;Ride.cpp']]],
  ['_5fcurrenttracklifthill_28158',['_currentTrackLiftHill',['../ride_2_ride_8cpp.html#a796c5aed028d9e74b7ee39f3f8bfbf88',1,'_currentTrackLiftHill():&#160;Ride.cpp'],['../_ride_8h.html#a796c5aed028d9e74b7ee39f3f8bfbf88',1,'_currentTrackLiftHill():&#160;Ride.cpp']]],
  ['_5fcurrenttrackpiecedirection_28159',['_currentTrackPieceDirection',['../ride_2_ride_8cpp.html#a15b2581da1626ff7c0b396062bf7a3c3',1,'_currentTrackPieceDirection():&#160;Ride.cpp'],['../_ride_8h.html#a15b2581da1626ff7c0b396062bf7a3c3',1,'_currentTrackPieceDirection():&#160;Ride.cpp']]],
  ['_5fcurrenttrackpiecetype_28160',['_currentTrackPieceType',['../ride_2_ride_8cpp.html#ad792ad4b9347756f342067fd0ba48625',1,'_currentTrackPieceType():&#160;Ride.cpp'],['../_ride_8h.html#ad792ad4b9347756f342067fd0ba48625',1,'_currentTrackPieceType():&#160;Ride.cpp']]],
  ['_5fcurrenttrackprice_28161',['_currentTrackPrice',['../ride_2_ride_8cpp.html#ad58c85abd168e80fc6fd22f28095f945',1,'_currentTrackPrice():&#160;Ride.cpp'],['../_ride_8h.html#ad58c85abd168e80fc6fd22f28095f945',1,'_currentTrackPrice():&#160;Ride.cpp']]],
  ['_5fcurrenttrackselectionflags_28162',['_currentTrackSelectionFlags',['../ride_2_ride_8cpp.html#ac98ff65ac5f909a9c9051f9d49d06744',1,'_currentTrackSelectionFlags():&#160;Ride.cpp'],['../_ride_8h.html#ac98ff65ac5f909a9c9051f9d49d06744',1,'_currentTrackSelectionFlags():&#160;Ride.cpp']]],
  ['_5fcurrenttrackslopeend_28163',['_currentTrackSlopeEnd',['../ride_2_ride_8cpp.html#a7e34a43b0376c711bcc2aa732cadeb32',1,'_currentTrackSlopeEnd():&#160;Ride.cpp'],['../_ride_8h.html#a7e34a43b0376c711bcc2aa732cadeb32',1,'_currentTrackSlopeEnd():&#160;Ride.cpp']]],
  ['_5fdeferclose_28164',['_deferClose',['../interface_2_window_8h.html#af1f92d3def17cdc666132496f0736a35',1,'_deferClose():&#160;_legacy.cpp'],['../__legacy_8cpp.html#af1f92d3def17cdc666132496f0736a35',1,'_deferClose():&#160;_legacy.cpp']]],
  ['_5fdirtygrid_28165',['_dirtyGrid',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a28c560ba58532bc5875c26a767bfaf73',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fdrawingcontext_28166',['_drawingContext',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a55c3c8346c108d9169acac9acb6e5310',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fenabledridepieces_28167',['_enabledRidePieces',['../interface_2_window_8h.html#a29019b1f71680ab7320b68e088a50a52',1,'_enabledRidePieces():&#160;_legacy.cpp'],['../__legacy_8cpp.html#a29019b1f71680ab7320b68e088a50a52',1,'_enabledRidePieces():&#160;_legacy.cpp']]],
  ['_5ffountainchanceofstoppingedgemode_28168',['_FountainChanceOfStoppingEdgeMode',['../_fountain_8cpp.html#aa72baa50a75050c0d6da3b1ee482ad9d',1,'Fountain.cpp']]],
  ['_5ffountainchanceofstoppingrandommode_28169',['_FountainChanceOfStoppingRandomMode',['../_fountain_8cpp.html#afbcb7c154aa2454bb041d5ca51b36de8',1,'Fountain.cpp']]],
  ['_5ffountaindirectionflags_28170',['_fountainDirectionFlags',['../_fountain_8cpp.html#a8ebaa53a63c4acf45c05384f39402a30',1,'Fountain.cpp']]],
  ['_5ffountaindirections_28171',['_fountainDirections',['../_fountain_8cpp.html#a2ea7a6ad6b742dc3394975ef9bbcbc4a',1,'Fountain.cpp']]],
  ['_5ffountainpatternflags_28172',['_fountainPatternFlags',['../_fountain_8cpp.html#a922cfadffaced54652387d787fcc593d',1,'Fountain.cpp']]],
  ['_5fframes_28173',['_frames',['../namespace_open_r_c_t2_1_1_paint.html#a529874df8e8552190c2f54ff3787f57e',1,'OpenRCT2::Paint']]],
  ['_5ffreepaintsessions_28174',['_freePaintSessions',['../namespace_open_r_c_t2_1_1_paint.html#a51f76493d2d8519c0310c42225ad52b9',1,'OpenRCT2::Paint']]],
  ['_5fgameactioncallbacks_28175',['_gameActionCallbacks',['../class_network.html#ae53f4d811ecbeb75a6ea33e43a49d6ff',1,'Network']]],
  ['_5fguestgenerationprobability_28176',['_guestGenerationProbability',['../world_2_park_8cpp.html#ad83f978b3457d6a56fa2d0f35e9076b9',1,'_guestGenerationProbability():&#160;Park.cpp'],['../_park_8h.html#ad83f978b3457d6a56fa2d0f35e9076b9',1,'_guestGenerationProbability():&#160;Park.cpp']]],
  ['_5fheight_28177',['_height',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a3819b58032adcebdd0c4a26fd541c85c',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fhooktableaddress_28178',['_hookTableAddress',['../_hook_8cpp.html#a1193981e7ec4806bf17831b98a0c3c7d',1,'Hook.cpp']]],
  ['_5fhooktableoffset_28179',['_hookTableOffset',['../_hook_8cpp.html#af728e4a270c1005fc79cb7fb8e906a65',1,'Hook.cpp']]],
  ['_5finputflags_28180',['_inputFlags',['../_input_8cpp.html#a0f2654a09af810c719bb02aeb150694b',1,'_inputFlags():&#160;Input.cpp'],['../_input_8h.html#a0f2654a09af810c719bb02aeb150694b',1,'_inputFlags():&#160;Input.cpp']]],
  ['_5finputstate_28181',['_inputState',['../_input_8cpp.html#a1bba4cda6a6ad7f938bec63250fa6f05',1,'_inputState():&#160;Input.cpp'],['../_input_8h.html#a1bba4cda6a6ad7f938bec63250fa6f05',1,'_inputState():&#160;Input.cpp']]],
  ['_5fkey_28182',['_key',['../class_network.html#a5d643c84fcfd3bdd84c03fd69ccf5a7f',1,'Network']]],
  ['_5flastsecond_28183',['_lastSecond',['../namespace_open_r_c_t2_1_1_paint.html#ad5be868a653ffa8e9121f7cbab67a6d4',1,'OpenRCT2::Paint']]],
  ['_5flog_5flevels_28184',['_log_levels',['../_diagnostic_8cpp.html#a10d7ecd919988fe23a572a9dea6135c8',1,'_log_levels():&#160;Diagnostic.cpp'],['../_diagnostic_8h.html#a10d7ecd919988fe23a572a9dea6135c8',1,'_log_levels():&#160;Diagnostic.cpp']]],
  ['_5fmaxhooks_28185',['_maxHooks',['../_hook_8cpp.html#a52e615ea4fa7bb78cc64c2cb09b29602',1,'Hook.cpp']]],
  ['_5fmaxobjectswashit_28186',['_maxObjectsWasHit',['../_editor_object_selection_session_8cpp.html#a354f92af486c6b6db647a7e80211d9ff',1,'_maxObjectsWasHit():&#160;EditorObjectSelectionSession.cpp'],['../_editor_object_selection_session_8h.html#a354f92af486c6b6db647a7e80211d9ff',1,'_maxObjectsWasHit():&#160;EditorObjectSelectionSession.cpp']]],
  ['_5fnumcurrentpossiblerideconfigurations_28187',['_numCurrentPossibleRideConfigurations',['../ride_2_ride_8cpp.html#aeabdbdf35a51c991a9a2904e746886fe',1,'_numCurrentPossibleRideConfigurations():&#160;Ride.cpp'],['../_ride_8h.html#aeabdbdf35a51c991a9a2904e746886fe',1,'_numCurrentPossibleRideConfigurations():&#160;Ride.cpp']]],
  ['_5fnumcurrentpossiblespecialtrackpieces_28188',['_numCurrentPossibleSpecialTrackPieces',['../ride_2_ride_8cpp.html#a371a7d917216c0c4ac103fb062e75556',1,'_numCurrentPossibleSpecialTrackPieces():&#160;Ride.cpp'],['../_ride_8h.html#a371a7d917216c0c4ac103fb062e75556',1,'_numCurrentPossibleSpecialTrackPieces():&#160;Ride.cpp']]],
  ['_5fnumselectedobjectsfortype_28189',['_numSelectedObjectsForType',['../_editor_object_selection_session_8cpp.html#ad13bd729fcd215b2ad17ccbaa1a2c34c',1,'_numSelectedObjectsForType():&#160;EditorObjectSelectionSession.cpp'],['../_editor_object_selection_session_8h.html#ad13bd729fcd215b2ad17ccbaa1a2c34c',1,'_numSelectedObjectsForType():&#160;EditorObjectSelectionSession.cpp']]],
  ['_5fobjectselectionflags_28190',['_objectSelectionFlags',['../_editor_object_selection_session_8cpp.html#a0cbfd5fd80d005a402937134c8c5db75',1,'_objectSelectionFlags():&#160;EditorObjectSelectionSession.cpp'],['../_editor_object_selection_session_8h.html#a0cbfd5fd80d005a402937134c8c5db75',1,'_objectSelectionFlags():&#160;EditorObjectSelectionSession.cpp']]],
  ['_5foriginaladdress_28191',['_originalAddress',['../_addresses_8cpp.html#a679e4bfdbef2364652ffb2fa3f23a41c',1,'Addresses.cpp']]],
  ['_5fpaintsessionpool_28192',['_paintSessionPool',['../namespace_open_r_c_t2_1_1_paint.html#ac4826aa1b8fa8b462e34ad59e8882d8b',1,'OpenRCT2::Paint']]],
  ['_5fpitch_28193',['_pitch',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a464855583c8efd6170149373eef7e72a',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fprevioustrackbankend_28194',['_previousTrackBankEnd',['../ride_2_ride_8cpp.html#aa87639cf596a5da4a1872e2450939d84',1,'_previousTrackBankEnd():&#160;Ride.cpp'],['../_ride_8h.html#aa87639cf596a5da4a1872e2450939d84',1,'_previousTrackBankEnd():&#160;Ride.cpp']]],
  ['_5fprevioustrackpiece_28195',['_previousTrackPiece',['../ride_2_ride_8cpp.html#a94836da6ec6d1dec5599f14979ee7263',1,'_previousTrackPiece():&#160;Ride.cpp'],['../_ride_8h.html#a94836da6ec6d1dec5599f14979ee7263',1,'_previousTrackPiece():&#160;Ride.cpp']]],
  ['_5fprevioustrackslopeend_28196',['_previousTrackSlopeEnd',['../ride_2_ride_8cpp.html#aed4de100cf2fc0504c3561a8a4de0787',1,'_previousTrackSlopeEnd():&#160;Ride.cpp'],['../_ride_8h.html#aed4de100cf2fc0504c3561a8a4de0787',1,'_previousTrackSlopeEnd():&#160;Ride.cpp']]],
  ['_5fraindrawer_28197',['_rainDrawer',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a6c0e81e260ce98977e82ef9236a77bbc',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5frideconstructionarrowpulsetime_28198',['_rideConstructionArrowPulseTime',['../ride_2_ride_8cpp.html#a016e9875f483253ac8c9a60008f0ac0a',1,'_rideConstructionArrowPulseTime():&#160;Ride.cpp'],['../_ride_8h.html#a016e9875f483253ac8c9a60008f0ac0a',1,'_rideConstructionArrowPulseTime():&#160;Ride.cpp']]],
  ['_5frideconstructionstate_28199',['_rideConstructionState',['../ride_2_ride_8cpp.html#a67003817783312803df847c76c6c3655',1,'_rideConstructionState():&#160;Ride.cpp'],['../_ride_8h.html#a67003817783312803df847c76c6c3655',1,'_rideConstructionState():&#160;Ride.cpp']]],
  ['_5frideconstructionstate2_28200',['_rideConstructionState2',['../interface_2_window_8h.html#ad9306852d2c46e8c9a5b8097357626b7',1,'_rideConstructionState2():&#160;_legacy.cpp'],['../__legacy_8cpp.html#ad9306852d2c46e8c9a5b8097357626b7',1,'_rideConstructionState2():&#160;_legacy.cpp']]],
  ['_5fselectedtracktype_28201',['_selectedTrackType',['../ride_2_ride_8cpp.html#a1250476507261fdaba276104ccd63540',1,'_selectedTrackType():&#160;Ride.cpp'],['../_ride_8h.html#a1250476507261fdaba276104ccd63540',1,'_selectedTrackType():&#160;Ride.cpp']]],
  ['_5fstandardpalette_28202',['_standardPalette',['../class_cmdline_sprite.html#a2174fbd9b14d07b22b67aa7841c5beef',1,'CmdlineSprite']]],
  ['_5fstationconstructed_28203',['_stationConstructed',['../interface_2_window_8h.html#a4cad5b0ca0f6f609ba1fbf43eab60a90',1,'_stationConstructed():&#160;_legacy.cpp'],['../__legacy_8cpp.html#a4cad5b0ca0f6f609ba1fbf43eab60a90',1,'_stationConstructed():&#160;_legacy.cpp']]],
  ['_5fsuggestedguestmaximum_28204',['_suggestedGuestMaximum',['../world_2_park_8cpp.html#a55c4e7e602fedf3deb8a6419261ce226',1,'_suggestedGuestMaximum():&#160;Park.cpp'],['../_park_8h.html#a55c4e7e602fedf3deb8a6419261ce226',1,'_suggestedGuestMaximum():&#160;Park.cpp']]],
  ['_5ftooltipnotshownticks_28205',['_tooltipNotShownTicks',['../_input_8cpp.html#a2d548b847c36b00299305b48894883c7',1,'_tooltipNotShownTicks():&#160;Input.cpp'],['../_input_8h.html#a2d548b847c36b00299305b48894883c7',1,'_tooltipNotShownTicks():&#160;Input.cpp']]],
  ['_5ftracksavedtileelements_28206',['_trackSavedTileElements',['../_track_design_8h.html#aeca2a603143abbc75e45340941cd92a5',1,'_trackSavedTileElements():&#160;TrackDesignSave.cpp'],['../_track_design_save_8cpp.html#aeca2a603143abbc75e45340941cd92a5',1,'_trackSavedTileElements():&#160;TrackDesignSave.cpp']]],
  ['_5ftracksavedtileelementsdesc_28207',['_trackSavedTileElementsDesc',['../_track_design_8h.html#a4099028e390491896203d9a051fab39a',1,'_trackSavedTileElementsDesc():&#160;TrackDesignSave.cpp'],['../_track_design_save_8cpp.html#a4099028e390491896203d9a051fab39a',1,'_trackSavedTileElementsDesc():&#160;TrackDesignSave.cpp']]],
  ['_5funkf440c5_28208',['_unkF440C5',['../ride_2_ride_8cpp.html#a2958a54e193b144870144b5a8e2d25fc',1,'_unkF440C5():&#160;Ride.cpp'],['../_ride_8h.html#a2958a54e193b144870144b5a8e2d25fc',1,'_unkF440C5():&#160;Ride.cpp']]],
  ['_5funkf44188_28209',['_unkF44188',['../ride_2_ride_8cpp.html#a73b96d923407ffe55b3469b96321e151',1,'_unkF44188():&#160;Ride.cpp'],['../_ride_8h.html#a73b96d923407ffe55b3469b96321e151',1,'_unkF44188():&#160;Ride.cpp']]],
  ['_5fusermanager_28210',['_userManager',['../class_network.html#a4a770d7d64a6ea5339cbfcadf99f0e4e',1,'Network']]],
  ['_5fvehiclebankendf64e37_28211',['_vehicleBankEndF64E37',['../_vehicle_8cpp.html#ae785bd404b685d6c3cf8cc08a001b57e',1,'_vehicleBankEndF64E37():&#160;Vehicle.cpp'],['../_vehicle_8h.html#ae785bd404b685d6c3cf8cc08a001b57e',1,'_vehicleBankEndF64E37():&#160;Vehicle.cpp']]],
  ['_5fvehiclef64e2c_28212',['_vehicleF64E2C',['../_vehicle_8cpp.html#ae4d9981e458f04877c2731883d6f1a54',1,'_vehicleF64E2C():&#160;Vehicle.cpp'],['../_vehicle_8h.html#ae4d9981e458f04877c2731883d6f1a54',1,'_vehicleF64E2C():&#160;Vehicle.cpp']]],
  ['_5fvehiclefrontvehicle_28213',['_vehicleFrontVehicle',['../_vehicle_8cpp.html#a7408acfa9cd30ada0dd6964059b5a904',1,'_vehicleFrontVehicle():&#160;Vehicle.cpp'],['../_vehicle_8h.html#a7408acfa9cd30ada0dd6964059b5a904',1,'_vehicleFrontVehicle():&#160;Vehicle.cpp']]],
  ['_5fvehiclemotiontrackflags_28214',['_vehicleMotionTrackFlags',['../_vehicle_8cpp.html#ade0f78cbd0e85d7c598a9cc8ecf3fc0f',1,'_vehicleMotionTrackFlags():&#160;Vehicle.cpp'],['../_vehicle_8h.html#ade0f78cbd0e85d7c598a9cc8ecf3fc0f',1,'_vehicleMotionTrackFlags():&#160;Vehicle.cpp']]],
  ['_5fvehiclestationindex_28215',['_vehicleStationIndex',['../_vehicle_8cpp.html#a7fb08abae9e2f1cbd60e2871c0f6f4d2',1,'_vehicleStationIndex():&#160;Vehicle.cpp'],['../_vehicle_8h.html#a7fb08abae9e2f1cbd60e2871c0f6f4d2',1,'_vehicleStationIndex():&#160;Vehicle.cpp']]],
  ['_5fvehicleunkf64e10_28216',['_vehicleUnkF64E10',['../_vehicle_8cpp.html#a8e04775ba4a431ea8d7da00b3e589ff5',1,'_vehicleUnkF64E10():&#160;Vehicle.cpp'],['../_vehicle_8h.html#a8e04775ba4a431ea8d7da00b3e589ff5',1,'_vehicleUnkF64E10():&#160;Vehicle.cpp']]],
  ['_5fvehiclevangleendf64e36_28217',['_vehicleVAngleEndF64E36',['../_vehicle_8cpp.html#aa36fe87cf05c66a95c2c22bb1852b1a8',1,'_vehicleVAngleEndF64E36():&#160;Vehicle.cpp'],['../_vehicle_8h.html#aa36fe87cf05c66a95c2c22bb1852b1a8',1,'_vehicleVAngleEndF64E36():&#160;Vehicle.cpp']]],
  ['_5fvehiclevelocityf64e08_28218',['_vehicleVelocityF64E08',['../_vehicle_8cpp.html#a917962a5b000d125d43d74d337e052d6',1,'_vehicleVelocityF64E08():&#160;Vehicle.cpp'],['../_vehicle_8h.html#a917962a5b000d125d43d74d337e052d6',1,'_vehicleVelocityF64E08():&#160;Vehicle.cpp']]],
  ['_5fvehiclevelocityf64e0c_28219',['_vehicleVelocityF64E0C',['../_vehicle_8cpp.html#a3bd7ee03ffafa8defd279ebd2770eb6d',1,'_vehicleVelocityF64E0C():&#160;Vehicle.cpp'],['../_vehicle_8h.html#a3bd7ee03ffafa8defd279ebd2770eb6d',1,'_vehicleVelocityF64E0C():&#160;Vehicle.cpp']]],
  ['_5fverbosity_28220',['_verbosity',['../test_2testpaint_2_main_8cpp.html#ad613e9b5646b8274f2cd45e2b6a4307a',1,'main.cpp']]],
  ['_5fwidth_28221',['_width',['../class_open_r_c_t2_1_1_drawing_1_1_x8_drawing_engine.html#a5d913abb7e59feda311ecee2ade6c36a',1,'OpenRCT2::Drawing::X8DrawingEngine']]],
  ['_5fwindow_5ftrack_5flist_5fitem_28222',['_window_track_list_item',['../_track_list_8cpp.html#a36e894c6866000d2a0e38e93e3b927c9',1,'_window_track_list_item():&#160;TrackList.cpp'],['../ui_2windows_2_window_8h.html#a36e894c6866000d2a0e38e93e3b927c9',1,'_window_track_list_item():&#160;TrackList.cpp']]]
];
