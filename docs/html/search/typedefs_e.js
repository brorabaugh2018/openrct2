var searchData=
[
  ['random_5fengine_5ft_32057',['random_engine_t',['../_scenario_8h.html#ac5baeeea58c53d4e9fcd67ca80d435bd',1,'Scenario.h']]],
  ['rct_5fexpenditure_5ftype_32058',['rct_expenditure_type',['../_finance_8h.html#a8e0410fb6e82c383333fe34b8165976f',1,'Finance.h']]],
  ['rct_5fstring_5fid_32059',['rct_string_id',['../common_8h.html#aaa72320362faee4f0ffedb18fcd2498d',1,'common.h']]],
  ['rct_5fwidgetindex_32060',['rct_widgetindex',['../interface_2_window_8h.html#a939adf1404e9899e8cb4857ef9219f48',1,'Window.h']]],
  ['rct_5fwindowclass_32061',['rct_windowclass',['../_context_8h.html#a0c195048e108f88f9ff8725225415a82',1,'rct_windowclass():&#160;Context.h'],['../interface_2_window_8h.html#a0c195048e108f88f9ff8725225415a82',1,'rct_windowclass():&#160;Window.h']]],
  ['rct_5fwindownumber_32062',['rct_windownumber',['../interface_2_window_8h.html#ada8f2056fd0f2e548e1b9646f7a1ecdc',1,'Window.h']]],
  ['rectcommandbatch_32063',['RectCommandBatch',['../_draw_commands_8h.html#af77914b390050501792243635bf174fb',1,'DrawCommands.h']]],
  ['reference_32064',['reference',['../class_circular_buffer.html#a894f0621891b5a49e22d88dd42f1cbf8',1,'CircularBuffer::reference()'],['../class_ride_manager_1_1_iterator.html#a93fafaf4e1c5d670a453dcf35acfb02f',1,'RideManager::Iterator::reference()']]],
  ['result_32065',['Result',['../struct_game_action_base.html#acc29eb8d6bfb3cfbca9b633df591fe36',1,'GameActionBase::Result()'],['../class_crypt_1_1_hash_algorithm.html#a5c0e94332772de78a2da1d8873225d1c',1,'Crypt::HashAlgorithm::Result()']]],
  ['result_5ftype_32066',['result_type',['../class_random_1_1_fixed_seed_sequence.html#a9984d7f889510e2bb9600ca80b82bff0',1,'Random::FixedSeedSequence::result_type()'],['../class_random_1_1_rotate_engine.html#ade3484ca842e4b2293338a8b850d6796',1,'Random::RotateEngine::result_type()']]],
  ['ride_5fid_5ft_32067',['ride_id_t',['../_ride_types_8h.html#a5038f1a184cb21f4c1acab02de594341',1,'RideTypes.h']]],
  ['ride_5fidnew_5ft_32068',['ride_idnew_t',['../_ride_types_8h.html#acb5b4bed7a259fd1b625fa6125081405',1,'RideTypes.h']]],
  ['ride_5frating_32069',['ride_rating',['../_ride_ratings_8h.html#a7968095b1b9c9aa9e04d32198804644d',1,'RideRatings.h']]],
  ['ride_5fratings_5fcalculation_32070',['ride_ratings_calculation',['../src_2openrct2_2ride_2_ride_ratings_8cpp.html#a06b87175d9b767cb8cc017aa61c2eb1d',1,'RideRatings.cpp']]]
];
