var searchData=
[
  ['gameaction_23117',['GameAction',['../struct_game_action.html',1,'']]],
  ['gameactionbase_23118',['GameActionBase',['../struct_game_action_base.html',1,'']]],
  ['gameactionnamequery_23119',['GameActionNameQuery',['../struct_game_action_name_query.html',1,'']]],
  ['gameactionresult_23120',['GameActionResult',['../class_game_action_result.html',1,'']]],
  ['gameactivity_23121',['GameActivity',['../classwebsite_1_1openrct2_1_1_game_activity.html',1,'website::openrct2']]],
  ['gamestate_23122',['GameState',['../class_open_r_c_t2_1_1_game_state.html',1,'OpenRCT2']]],
  ['gamestatecomparedata_5ft_23123',['GameStateCompareData_t',['../struct_game_state_compare_data__t.html',1,'']]],
  ['gamestatesnapshot_5ft_23124',['GameStateSnapshot_t',['../struct_game_state_snapshot__t.html',1,'']]],
  ['gamestatesnapshots_23125',['GameStateSnapshots',['../struct_game_state_snapshots.html',1,'']]],
  ['gamestatespritechange_5ft_23126',['GameStateSpriteChange_t',['../struct_game_state_sprite_change__t.html',1,'']]],
  ['generalconfiguration_23127',['GeneralConfiguration',['../struct_general_configuration.html',1,'']]],
  ['gforces_23128',['GForces',['../struct_g_forces.html',1,'']]],
  ['glyphid_23129',['GlyphId',['../struct_glyph_id.html',1,'']]],
  ['guest_23130',['Guest',['../struct_guest.html',1,'']]]
];
