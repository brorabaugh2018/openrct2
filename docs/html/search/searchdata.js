var indexSectionsWithContent =
{
  0: "!$3_abcdefghijklmnopqrstuvwxyz~ÅàáäåæéöøúüČőГОПбвгдежзиклнпрстучэまパ並乐体你例含将將尚并我拥文樂每現生第設设这這通選飲饮體가거그낙내모생속수시음이입저전정최",
  1: "_abcdefghijklmnopqrstuvwxyz",
  2: "cdefgijlmnoprstuvwz",
  3: "3_abcdefghijklmnoprstuvwxz",
  4: "_abcdefghijklmnopqrstuvwxz~áс並并設選전",
  5: "!$_abcdefghijklmnopqrstuvwxyzÅàáäåæéöøúüČőГОПбвгдежзиклнпрстучэまパ並乐体你例含将將尚并我拥文樂每現生第设这這通飲饮體가거그낙내모생속수시음이입저정최",
  6: "abcdefghilmnoprstuvw",
  7: "abcdefgijklmnoprstvwz",
  8: "abcdefghijklmnopqrstuvwxy",
  9: "o",
  10: "_abcdefghiklmnoprstuvwxy",
  11: "cosw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

