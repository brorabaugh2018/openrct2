var searchData=
[
  ['s6_5fmagic_5fnumber_46037',['S6_MAGIC_NUMBER',['../_scenario_8h.html#ac27cb3de1c55ccf532c69602402fae66',1,'Scenario.h']]],
  ['s6_5frct2_5fversion_46038',['S6_RCT2_VERSION',['../_scenario_8h.html#a32ee2b561f3dc6b8231467d1579ee844',1,'Scenario.h']]],
  ['safedelete_46039',['SafeDelete',['../common_8h.html#a1575ebd9197c510ef46c1b2c02afab53',1,'common.h']]],
  ['safedeletearray_46040',['SafeDeleteArray',['../common_8h.html#ab30b87bf831c3a985387b20b17372543',1,'common.h']]],
  ['safefree_46041',['SafeFree',['../common_8h.html#a9ab68c00c3ba52f677a5e0a8eafe992c',1,'common.h']]],
  ['sce_5fgbdb_46042',['SCE_GBDB',['../ui_2windows_2_tile_inspector_8cpp.html#af715ee4d14faba3bde2b18eb98b68f9d',1,'TileInspector.cpp']]],
  ['sce_5fgbdt_46043',['SCE_GBDT',['../ui_2windows_2_tile_inspector_8cpp.html#a5948e1a0b173f30d2816276fbe9087cc',1,'TileInspector.cpp']]],
  ['sce_5fgbpb_46044',['SCE_GBPB',['../ui_2windows_2_tile_inspector_8cpp.html#a3683c0f4445e88ee3a05165d372ed09c',1,'TileInspector.cpp']]],
  ['sce_5fgbpt_46045',['SCE_GBPT',['../ui_2windows_2_tile_inspector_8cpp.html#aee38cd2b656889497120226e0a69bbaa',1,'TileInspector.cpp']]],
  ['scenario_5foptions_5fstart_46046',['SCENARIO_OPTIONS_START',['../_options_8cpp.html#a0c3c3b54aec18657d6a5e456b114ecb1',1,'Options.cpp']]],
  ['scenario_5fstart_46047',['SCENARIO_START',['../_options_8cpp.html#ae61485b244d6f80e397d2e9c3a9575b9',1,'Options.cpp']]],
  ['scenery_5fbanners_5fid_5fmax_46048',['SCENERY_BANNERS_ID_MAX',['../_scenery_8h.html#a89b39e51f4af1ad748050eed721d16ee',1,'Scenery.h']]],
  ['scenery_5fbanners_5fid_5fmin_46049',['SCENERY_BANNERS_ID_MIN',['../_scenery_8h.html#a6bc30920d93585dc865a4f9227390ecc',1,'Scenery.h']]],
  ['scenery_5fbutton_5fheight_46050',['SCENERY_BUTTON_HEIGHT',['../ui_2windows_2_scenery_8cpp.html#a92ac11b6882d00ea9f40273bc93ffa55',1,'Scenery.cpp']]],
  ['scenery_5fbutton_5fwidth_46051',['SCENERY_BUTTON_WIDTH',['../ui_2windows_2_scenery_8cpp.html#abfc71a7e0855c4b1b053a348d7707327',1,'Scenery.cpp']]],
  ['scenery_5fentries_5fby_5ftab_46052',['SCENERY_ENTRIES_BY_TAB',['../_scenery_8h.html#ad25e0e5df53e996e56575c3474974bea',1,'Scenery.h']]],
  ['scenery_5flarge_5fscenery_5fid_5fmax_46053',['SCENERY_LARGE_SCENERY_ID_MAX',['../_scenery_8h.html#a4569cbe130d0b2021ffb4a971b000d63',1,'Scenery.h']]],
  ['scenery_5flarge_5fscenery_5fid_5fmin_46054',['SCENERY_LARGE_SCENERY_ID_MIN',['../_scenery_8h.html#a55aba2893e9bfba4a6780eb3a2740322',1,'Scenery.h']]],
  ['scenery_5fpath_5fscenery_5fid_5fmax_46055',['SCENERY_PATH_SCENERY_ID_MAX',['../_scenery_8h.html#a7a9e95765ace1b51a8260d3ad8fdb005',1,'Scenery.h']]],
  ['scenery_5fpath_5fscenery_5fid_5fmin_46056',['SCENERY_PATH_SCENERY_ID_MIN',['../_scenery_8h.html#a18922a519aa95b2a1918ee770465341c',1,'Scenery.h']]],
  ['scenery_5fsmall_5fscenery_5fid_5fmax_46057',['SCENERY_SMALL_SCENERY_ID_MAX',['../_scenery_8h.html#a03f856f672606e4dcf37d48615f3b955',1,'Scenery.h']]],
  ['scenery_5fsmall_5fscenery_5fid_5fmin_46058',['SCENERY_SMALL_SCENERY_ID_MIN',['../_scenery_8h.html#a492e10bd73c01d39d985272c4f069616',1,'Scenery.h']]],
  ['scenery_5fwalls_5fid_5fmax_46059',['SCENERY_WALLS_ID_MAX',['../_scenery_8h.html#a97ad925625fb20989c799d62171c6bff',1,'Scenery.h']]],
  ['scenery_5fwalls_5fid_5fmin_46060',['SCENERY_WALLS_ID_MIN',['../_scenery_8h.html#abe4b3758e745ba31ab05c43d7731e266',1,'Scenery.h']]],
  ['scenery_5fwindow_5ftabs_46061',['SCENERY_WINDOW_TABS',['../ui_2windows_2_scenery_8cpp.html#a5848bd5b1d2d4639e361d0dd31addf9e',1,'Scenery.cpp']]],
  ['scenery_5fwither_5fage_5fthreshold_5f1_46062',['SCENERY_WITHER_AGE_THRESHOLD_1',['../_scenery_8h.html#aea25cedd75d033697d4a3ff44af7e78a',1,'Scenery.h']]],
  ['scenery_5fwither_5fage_5fthreshold_5f2_46063',['SCENERY_WITHER_AGE_THRESHOLD_2',['../_scenery_8h.html#a5aace57e5884ec83d740378cdeff2ac1',1,'Scenery.h']]],
  ['scroll_5fpos_46064',['SCROLL_POS',['../_scrolling_text_8cpp.html#aa996e8179688eae5a3fe3a75622b9c41',1,'ScrollingText.cpp']]],
  ['scroll_5fwidth_46065',['SCROLL_WIDTH',['../_title_editor_8cpp.html#a27dc12e70c90b8af8d7fff68e6fba901',1,'TitleEditor.cpp']]],
  ['scrollable_5frow_5fheight_46066',['SCROLLABLE_ROW_HEIGHT',['../interface_2_window_8h.html#a429f3b9ec2e969ec64ded8b1df9730f5',1,'Window.h']]],
  ['scrollbar_5fsize_46067',['SCROLLBAR_SIZE',['../interface_2_window_8h.html#a33e75ceb10cc31e7e53156ade2c44cd6',1,'Window.h']]],
  ['selected_5fride_5fundefined_46068',['SELECTED_RIDE_UNDEFINED',['../_new_campaign_8cpp.html#a9a9301472af58edafc8c4df0332461c2',1,'NewCampaign.cpp']]],
  ['set_5fflag_46069',['SET_FLAG',['../_interactive_console_8cpp.html#a631575023fd73d5c0262b63c56cb537c',1,'InteractiveConsole.cpp']]],
  ['set_5fformat_5farg_46070',['set_format_arg',['../_localisation_8h.html#a48ff9eaa71414a8eef803dc49c0e8484',1,'Localisation.h']]],
  ['set_5fformat_5farg_5fon_46071',['set_format_arg_on',['../_localisation_8h.html#a946d0e38be1ca681ade8091d505a47cd',1,'Localisation.h']]],
  ['set_5fmap_5ftooltip_5fformat_5farg_46072',['set_map_tooltip_format_arg',['../_localisation_8h.html#a271fd610a1b3b1de27bfa99545414246',1,'Localisation.h']]],
  ['shared_5fwidgets_46073',['SHARED_WIDGETS',['../ui_2windows_2_map_gen_8cpp.html#a1aae650043d98da511d477e84c346ede',1,'MapGen.cpp']]],
  ['shift_46074',['SHIFT',['../_keyboard_shortcuts_8h.html#ac179eef68bcc694aa0ef8dd1eb09950b',1,'KeyboardShortcuts.h']]],
  ['socket_5ferror_46075',['SOCKET_ERROR',['../_socket_8cpp.html#a633b0396ff93d336a088412a190a5072',1,'Socket.cpp']]],
  ['sound_5fid_5fnull_46076',['SOUND_ID_NULL',['../audio_8h.html#a8ae958adc2a37d8571bdff4f38d1310b',1,'audio.h']]],
  ['source_5fcol_5fleft_46077',['SOURCE_COL_LEFT',['../_object_load_error_8cpp.html#a9e883fe6b6f3572d4add5aee9127ca8b',1,'ObjectLoadError.cpp']]],
  ['spatial_5findex_5flocation_5fnull_46078',['SPATIAL_INDEX_LOCATION_NULL',['../_sprite_8cpp.html#a4cb86838fa01dafe9d3976823de0ced6',1,'Sprite.cpp']]],
  ['spinner_5fdecrease_46079',['SPINNER_DECREASE',['../interface_2_widget_8h.html#ad1208b43b7bd4c8eb3d2d55e3b573e40',1,'Widget.h']]],
  ['spinner_5fincrease_46080',['SPINNER_INCREASE',['../interface_2_widget_8h.html#a3fce5ff1b2472c949a9ba98bc064ae0e',1,'Widget.h']]],
  ['spinner_5fwidgets_46081',['SPINNER_WIDGETS',['../interface_2_widget_8h.html#ad3f15a0e4eedb4125efe2de8773e4b3e',1,'Widget.h']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f0_5fa_46082',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#ad10a7c1b166ca2f9275cd49037e0c735',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f0_5fb_46083',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_0_B',['../_side_friction_roller_coaster_8cpp.html#a7a73fd248a17c7f00106f5694df79a24',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f1_5fa_46084',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#af4742d75643879006bcca8109aaa3b67',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f1_5fb_46085',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_1_B',['../_side_friction_roller_coaster_8cpp.html#ab4f04bfa17b0f8f235c856d148b8b387',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f2_5fa_46086',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#a2f8af1475c82351fcefcdba9a1e8aeb0',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f2_5fb_46087',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_2_B',['../_side_friction_roller_coaster_8cpp.html#a2208c6ca89e1e4c9522c5bbc3a6378ff',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f3_5fa_46088',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#aedc0adbe12572c0b1e9a18bd8f339332',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f3_5fb_46089',['SPR_SIDE_FRICTION_25_DEG_UP_TO_60_DEG_UP_DIR_3_B',['../_side_friction_roller_coaster_8cpp.html#accc9616d3521a32e69db4fef3cdeef39',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f0_5fa_46090',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#a10cefe29d68e487fd05a73277878d5c7',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f0_5fb_46091',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_0_B',['../_side_friction_roller_coaster_8cpp.html#a679300f781291cb40479f0d5112f1b5b',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f1_5fa_46092',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#ae21cd77203eed09f49cc9fec9237c896',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f1_5fb_46093',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_1_B',['../_side_friction_roller_coaster_8cpp.html#a15ea0f3c7b601b366a687234f8c1cedc',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f2_5fa_46094',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#abf9ec70e19f5db397e438e289202b33a',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f2_5fb_46095',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_2_B',['../_side_friction_roller_coaster_8cpp.html#a072b289fc09147246026dfcfcf775238',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f3_5fa_46096',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#af5bc0250b372118767c890ff273ef734',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fdir_5f3_5fb_46097',['SPR_SIDE_FRICTION_60_DEG_UP_DIR_3_B',['../_side_friction_roller_coaster_8cpp.html#ad766de4726d5d57083702c42b3e6a402',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f0_5fa_46098',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#aec4936a9d611ae05c883f745ca423255',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f0_5fb_46099',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_0_B',['../_side_friction_roller_coaster_8cpp.html#a8eded91a87ded8d4e4863d2f9fa76325',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f1_5fa_46100',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#a64414ad6c40ca973b0f6e6d2cb4d2f54',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f1_5fb_46101',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_1_B',['../_side_friction_roller_coaster_8cpp.html#abd87530667f718c8c13854f9de33300a',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f2_5fa_46102',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#afc2200ef83addfe4e118c0492c2b5d5f',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f2_5fb_46103',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_2_B',['../_side_friction_roller_coaster_8cpp.html#ac9385821757188a8b8f6f2b80b8cfd58',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f3_5fa_46104',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#aa22853516c52a8e5411e21df682dced4',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f3_5fb_46105',['SPR_SIDE_FRICTION_60_DEG_UP_TO_25_DEG_UP_DIR_3_B',['../_side_friction_roller_coaster_8cpp.html#aa472ea84276bac5914f982ba9ed608fa',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f0_5fa_46106',['SPR_SIDE_FRICTION_DIAG_25_DEG_UP_TO_60_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#aa581660f2c3b795abc1f7ea68cc3dd9f',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f1_5fa_46107',['SPR_SIDE_FRICTION_DIAG_25_DEG_UP_TO_60_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#aebfc47884beec11da7b48769ea75b5b3',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f2_5fa_46108',['SPR_SIDE_FRICTION_DIAG_25_DEG_UP_TO_60_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#ae4cce19abc65fa09aa4e62c60de3764e',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f25_5fdeg_5fup_5fto_5f60_5fdeg_5fup_5fdir_5f3_5fa_46109',['SPR_SIDE_FRICTION_DIAG_25_DEG_UP_TO_60_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#a739d760e13e4ac5a03f4dafe7f031e32',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f0_5fa_46110',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#ab8834c06543d4dea7be9c727bf06d098',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f0_5fb_46111',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_0_B',['../_side_friction_roller_coaster_8cpp.html#aafa65d851df0768484a99190eb3a7d69',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f1_5fa_46112',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#af2302778b800ab4ab861e66b212c5808',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f2_5fa_46113',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#a895a259c32e705b73d9633a6c1b9f981',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f2_5fb_46114',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_2_B',['../_side_friction_roller_coaster_8cpp.html#a55216669dddf7a4a4c017689b3c89bbd',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fdir_5f3_5fa_46115',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#ac422f1464950cc6bfd060c20197b776f',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f0_5fa_46116',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_0_A',['../_side_friction_roller_coaster_8cpp.html#a670c9f4b57b7d6bd55ff109fe3d8eded',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f0_5fb_46117',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_0_B',['../_side_friction_roller_coaster_8cpp.html#aa473a3a4078fd5d0bc26cd4f83cb6917',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f1_5fa_46118',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_1_A',['../_side_friction_roller_coaster_8cpp.html#a3cbb75ba112ca885b592e4b52053a906',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f2_5fa_46119',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_2_A',['../_side_friction_roller_coaster_8cpp.html#a6a13c5616378dfb5a568012ba6099a64',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f2_5fb_46120',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_2_B',['../_side_friction_roller_coaster_8cpp.html#a70aa29a58fac80b2decfe1f24f8037ad',1,'SideFrictionRollerCoaster.cpp']]],
  ['spr_5fside_5ffriction_5fdiag_5f60_5fdeg_5fup_5fto_5f25_5fdeg_5fup_5fdir_5f3_5fa_46121',['SPR_SIDE_FRICTION_DIAG_60_DEG_UP_TO_25_DEG_UP_DIR_3_A',['../_side_friction_roller_coaster_8cpp.html#a668001a58a82d3e62057466da3113e54',1,'SideFrictionRollerCoaster.cpp']]],
  ['sprite_5fid_5fpalette_5fcolour_5f1_46122',['SPRITE_ID_PALETTE_COLOUR_1',['../_drawing_8h.html#a2bdbbc7eb571a01a295b562ae58099c4',1,'Drawing.h']]],
  ['sprite_5fid_5fpalette_5fcolour_5f2_46123',['SPRITE_ID_PALETTE_COLOUR_2',['../_drawing_8h.html#afb01dd2aa8f88d1fa2982b07b6bf50ec',1,'Drawing.h']]],
  ['sprite_5fid_5fpalette_5fcolour_5f3_46124',['SPRITE_ID_PALETTE_COLOUR_3',['../_drawing_8h.html#af78ef8da4cfa55d6a81aec0dce1015c0',1,'Drawing.h']]],
  ['sprite_5findex_5fnull_46125',['SPRITE_INDEX_NULL',['../_sprite_8h.html#ae0bd8a0091412847771558140f24901f',1,'Sprite.h']]],
  ['staff_5fmax_5fcount_46126',['STAFF_MAX_COUNT',['../_staff_8h.html#a663f15f031bc443ca59b63f414ccc05c',1,'Staff.h']]],
  ['staff_5fpatrol_5farea_5fsize_46127',['STAFF_PATROL_AREA_SIZE',['../_staff_8h.html#a2941259b2109da79051370c798a008b2',1,'Staff.h']]],
  ['station_5fdepart_5fflag_46128',['STATION_DEPART_FLAG',['../_ride_8h.html#a2f3700894ec86262faedfb6339fe2967',1,'Ride.h']]],
  ['station_5fdepart_5fmask_46129',['STATION_DEPART_MASK',['../_ride_8h.html#a068629696544f1dfb2d3bb902e5f81d8',1,'Ride.h']]],
  ['str_5fnone_46130',['STR_NONE',['../_string_ids_8h.html#afe31e7691d6ff56f21f4762bf899af2d',1,'StringIds.h']]],
  ['str_5fviewport_46131',['STR_VIEWPORT',['../_string_ids_8h.html#a65a91b5b444f92067aa190452c8778ab',1,'StringIds.h']]],
  ['sur_5fgbdb_46132',['SUR_GBDB',['../ui_2windows_2_tile_inspector_8cpp.html#a783d066b8ed72b491a461b7bef5c7215',1,'TileInspector.cpp']]],
  ['sur_5fgbdt_46133',['SUR_GBDT',['../ui_2windows_2_tile_inspector_8cpp.html#a1e40bfe950a923e96a172d7e7342a783',1,'TileInspector.cpp']]],
  ['sur_5fgbpb_46134',['SUR_GBPB',['../ui_2windows_2_tile_inspector_8cpp.html#afcd8e48fe933ce96c6a1e0cd7812ebef',1,'TileInspector.cpp']]],
  ['sur_5fgbpt_46135',['SUR_GBPT',['../ui_2windows_2_tile_inspector_8cpp.html#ade09745ae0cf7ef574172d0abbd979dd',1,'TileInspector.cpp']]],
  ['synchronised_5fvehicle_5fcount_46136',['SYNCHRONISED_VEHICLE_COUNT',['../_vehicle_8cpp.html#a5e9eb4f79db8c540988628f7f1ff65a0',1,'Vehicle.cpp']]],
  ['sz_5fclosest_46137',['SZ_CLOSEST',['../_sprite_commands_8cpp.html#a265796071926ff5cb4631731dde80902',1,'SpriteCommands.cpp']]],
  ['sz_5fdefault_46138',['SZ_DEFAULT',['../_sprite_commands_8cpp.html#a97562c181890e600a888e5e8f716b358',1,'SpriteCommands.cpp']]],
  ['sz_5fdithering_46139',['SZ_DITHERING',['../_sprite_commands_8cpp.html#a3579033e38bf063e170e1ad4f8d0bc10',1,'SpriteCommands.cpp']]]
];
