var searchData=
[
  ['railingentrysupporttype_32192',['RailingEntrySupportType',['../_footpath_8h.html#a5307eb391f46733d0a78d3f8018ed95d',1,'Footpath.h']]],
  ['rain_5flevel_32193',['RAIN_LEVEL',['../_climate_8h.html#a562df253c448475c1d049569061ee948',1,'Climate.h']]],
  ['rct12tileelementtype_32194',['RCT12TileElementType',['../_r_c_t12_8h.html#a59bff1292891d44436b9461101cda2b0',1,'RCT12.h']]],
  ['rct12trackdesignversion_32195',['RCT12TrackDesignVersion',['../_r_c_t12_8h.html#a10fc825735c0567a8b1dfde82f9dfb66',1,'RCT12.h']]],
  ['rct1_5fpeep_5fsprite_5ftype_32196',['RCT1_PEEP_SPRITE_TYPE',['../_r_c_t1_8h.html#a9e654635ffda709b48c09a925868f290',1,'RCT1.h']]],
  ['rct2_5feditor_5fstep_32197',['RCT2_EDITOR_STEP',['../_editor_8h.html#ac4c40bd0f6a56e0b397e7a77d1fdb533',1,'Editor.h']]],
  ['rct2languageid_32198',['RCT2LanguageId',['../_language_8h.html#a60536d17af0be7bcd100a48635be51bb',1,'Language.h']]],
  ['ride_5fcomponent_5ftype_32199',['RIDE_COMPONENT_TYPE',['../_ride_data_8h.html#ad6984043696a888fffb7f8de0f20be61',1,'RideData.h']]],
  ['ride_5ftype_5fflags_32200',['ride_type_flags',['../_ride_8h.html#aa00a9bb1b51a80116527a181cc7da5ad',1,'Ride.h']]],
  ['rideclassification_32201',['RideClassification',['../_ride_8h.html#a532b3ecfcb17f091988441b2239edb4f',1,'Ride.h']]],
  ['ridegroupflags_32202',['RideGroupFlags',['../_ride_group_manager_8h.html#ac3cfd029bf9c2fcd4ac4bdfa6b70c29c',1,'RideGroupManager.h']]],
  ['ridesetappearancetype_32203',['RideSetAppearanceType',['../_ride_set_appearance_action_8hpp.html#a0faea848c4b7610e85bf4620655bd7e1',1,'RideSetAppearanceAction.hpp']]],
  ['ridesetsetting_32204',['RideSetSetting',['../_ride_set_setting_8hpp.html#a387cceed24c652adc60c667bb72a6ca6',1,'RideSetSetting.hpp']]],
  ['ridesetvehicletype_32205',['RideSetVehicleType',['../_ride_set_vehicles_action_8hpp.html#ad8c3d6f0c49335fc6eca085c1e8660dc',1,'RideSetVehiclesAction.hpp']]]
];
